#include "simpleifc/ifc2x3.h"

using namespace simpleifc;

const Schema& simpleifc::ifc2x3::schema()
{
    static Schema schema("IFC2X3");
    static bool initialized = false;
    if ( !initialized )
    {
        initialized = true;
        schema.define(
            "IfcActorRole",
            nullptr,
            {
                {"Role"},
                {"UserDefinedRole"},
                {"Description"},
                
            }
        );
        CoreClass* IfcAddress = schema.define(
            "IfcAddress",
            nullptr,
            {
                {"Purpose"},
                {"Description"},
                {"UserDefinedPurpose"},
                
            }
        );
        schema.define(
            "IfcApplication",
            nullptr,
            {
                {"ApplicationDeveloper"},
                {"Version"},
                {"ApplicationFullName"},
                {"ApplicationIdentifier"},
                
            }
        );
        CoreClass* IfcAppliedValue = schema.define(
            "IfcAppliedValue",
            nullptr,
            {
                {"Name"},
                {"Description"},
                {"AppliedValue"},
                {"UnitBasis"},
                {"ApplicableDate"},
                {"FixedUntilDate"},
                
            }
        );
        schema.define(
            "IfcAppliedValueRelationship",
            nullptr,
            {
                {"ComponentOfTotal"},
                {"Components"},
                {"ArithmeticOperator"},
                {"Name"},
                {"Description"},
                
            }
        );
        schema.define(
            "IfcApproval",
            nullptr,
            {
                {"Description"},
                {"ApprovalDateTime"},
                {"ApprovalStatus"},
                {"ApprovalLevel"},
                {"ApprovalQualifier"},
                {"Name"},
                {"Identifier"},
                
            }
        );
        schema.define(
            "IfcApprovalActorRelationship",
            nullptr,
            {
                {"Actor"},
                {"Approval"},
                {"Role"},
                
            }
        );
        schema.define(
            "IfcApprovalPropertyRelationship",
            nullptr,
            {
                {"ApprovedProperties"},
                {"Approval"},
                
            }
        );
        schema.define(
            "IfcApprovalRelationship",
            nullptr,
            {
                {"RelatedApproval"},
                {"RelatingApproval"},
                {"Description"},
                {"Name"},
                
            }
        );
        CoreClass* IfcBoundaryCondition = schema.define(
            "IfcBoundaryCondition",
            nullptr,
            {
                {"Name"},
                
            }
        );
        schema.define(
            "IfcBoundaryEdgeCondition",
            IfcBoundaryCondition,
            {
                {"LinearStiffnessByLengthX"},
                {"LinearStiffnessByLengthY"},
                {"LinearStiffnessByLengthZ"},
                {"RotationalStiffnessByLengthX"},
                {"RotationalStiffnessByLengthY"},
                {"RotationalStiffnessByLengthZ"},
                
            }
        );
        schema.define(
            "IfcBoundaryFaceCondition",
            IfcBoundaryCondition,
            {
                {"LinearStiffnessByAreaX"},
                {"LinearStiffnessByAreaY"},
                {"LinearStiffnessByAreaZ"},
                
            }
        );
        CoreClass* IfcBoundaryNodeCondition = schema.define(
            "IfcBoundaryNodeCondition",
            IfcBoundaryCondition,
            {
                {"LinearStiffnessX"},
                {"LinearStiffnessY"},
                {"LinearStiffnessZ"},
                {"RotationalStiffnessX"},
                {"RotationalStiffnessY"},
                {"RotationalStiffnessZ"},
                
            }
        );
        schema.define(
            "IfcBoundaryNodeConditionWarping",
            IfcBoundaryNodeCondition,
            {
                {"WarpingStiffness"},
                
            }
        );
        schema.define(
            "IfcCalendarDate",
            nullptr,
            {
                {"DayComponent"},
                {"MonthComponent"},
                {"YearComponent"},
                
            }
        );
        schema.define(
            "IfcClassification",
            nullptr,
            {
                {"Source"},
                {"Edition"},
                {"EditionDate"},
                {"Name"},
                
            }
        );
        schema.define(
            "IfcClassificationItem",
            nullptr,
            {
                {"Notation"},
                {"ItemOf"},
                {"Title"},
                
            }
        );
        schema.define(
            "IfcClassificationItemRelationship",
            nullptr,
            {
                {"RelatingItem"},
                {"RelatedItems"},
                
            }
        );
        schema.define(
            "IfcClassificationNotation",
            nullptr,
            {
                {"NotationFacets"},
                
            }
        );
        schema.define(
            "IfcClassificationNotationFacet",
            nullptr,
            {
                {"NotationValue"},
                
            }
        );
        CoreClass* IfcColourSpecification = schema.define(
            "IfcColourSpecification",
            nullptr,
            {
                {"Name"},
                
            }
        );
        CoreClass* IfcConnectionGeometry = schema.define(
            "IfcConnectionGeometry",
            nullptr,
            {
                
            }
        );
        CoreClass* IfcConnectionPointGeometry = schema.define(
            "IfcConnectionPointGeometry",
            IfcConnectionGeometry,
            {
                {"PointOnRelatingElement"},
                {"PointOnRelatedElement"},
                
            }
        );
        schema.define(
            "IfcConnectionPortGeometry",
            IfcConnectionGeometry,
            {
                {"LocationAtRelatingElement"},
                {"LocationAtRelatedElement"},
                {"ProfileOfPort"},
                
            }
        );
        schema.define(
            "IfcConnectionSurfaceGeometry",
            IfcConnectionGeometry,
            {
                {"SurfaceOnRelatingElement"},
                {"SurfaceOnRelatedElement"},
                
            }
        );
        CoreClass* IfcConstraint = schema.define(
            "IfcConstraint",
            nullptr,
            {
                {"Name"},
                {"Description"},
                {"ConstraintGrade"},
                {"ConstraintSource"},
                {"CreatingActor"},
                {"CreationTime"},
                {"UserDefinedGrade"},
                
            }
        );
        schema.define(
            "IfcConstraintAggregationRelationship",
            nullptr,
            {
                {"Name"},
                {"Description"},
                {"RelatingConstraint"},
                {"RelatedConstraints"},
                {"LogicalAggregator"},
                
            }
        );
        schema.define(
            "IfcConstraintClassificationRelationship",
            nullptr,
            {
                {"ClassifiedConstraint"},
                {"RelatedClassifications"},
                
            }
        );
        schema.define(
            "IfcConstraintRelationship",
            nullptr,
            {
                {"Name"},
                {"Description"},
                {"RelatingConstraint"},
                {"RelatedConstraints"},
                
            }
        );
        schema.define(
            "IfcCoordinatedUniversalTimeOffset",
            nullptr,
            {
                {"HourOffset"},
                {"MinuteOffset"},
                {"Sense"},
                
            }
        );
        schema.define(
            "IfcCostValue",
            IfcAppliedValue,
            {
                {"CostType"},
                {"Condition"},
                
            }
        );
        schema.define(
            "IfcCurrencyRelationship",
            nullptr,
            {
                {"RelatingMonetaryUnit"},
                {"RelatedMonetaryUnit"},
                {"ExchangeRate"},
                {"RateDateTime"},
                {"RateSource"},
                
            }
        );
        schema.define(
            "IfcCurveStyleFont",
            nullptr,
            {
                {"Name"},
                {"PatternList"},
                
            }
        );
        schema.define(
            "IfcCurveStyleFontAndScaling",
            nullptr,
            {
                {"Name"},
                {"CurveFont"},
                {"CurveFontScaling"},
                
            }
        );
        schema.define(
            "IfcCurveStyleFontPattern",
            nullptr,
            {
                {"VisibleSegmentLength"},
                {"InvisibleSegmentLength"},
                
            }
        );
        schema.define(
            "IfcDateAndTime",
            nullptr,
            {
                {"DateComponent"},
                {"TimeComponent"},
                
            }
        );
        schema.define(
            "IfcDerivedUnit",
            nullptr,
            {
                {"Elements"},
                {"UnitType"},
                {"UserDefinedType"},
                
            }
        );
        schema.define(
            "IfcDerivedUnitElement",
            nullptr,
            {
                {"Unit"},
                {"Exponent"},
                
            }
        );
        schema.define(
            "IfcDimensionalExponents",
            nullptr,
            {
                {"LengthExponent"},
                {"MassExponent"},
                {"TimeExponent"},
                {"ElectricCurrentExponent"},
                {"ThermodynamicTemperatureExponent"},
                {"AmountOfSubstanceExponent"},
                {"LuminousIntensityExponent"},
                
            }
        );
        schema.define(
            "IfcDocumentElectronicFormat",
            nullptr,
            {
                {"FileExtension"},
                {"MimeContentType"},
                {"MimeSubtype"},
                
            }
        );
        schema.define(
            "IfcDocumentInformation",
            nullptr,
            {
                {"DocumentId"},
                {"Name"},
                {"Description"},
                {"DocumentReferences"},
                {"Purpose"},
                {"IntendedUse"},
                {"Scope"},
                {"Revision"},
                {"DocumentOwner"},
                {"Editors"},
                {"CreationTime"},
                {"LastRevisionTime"},
                {"ElectronicFormat"},
                {"ValidFrom"},
                {"ValidUntil"},
                {"Confidentiality"},
                {"Status"},
                
            }
        );
        schema.define(
            "IfcDocumentInformationRelationship",
            nullptr,
            {
                {"RelatingDocument"},
                {"RelatedDocuments"},
                {"RelationshipType"},
                
            }
        );
        CoreClass* IfcDraughtingCalloutRelationship = schema.define(
            "IfcDraughtingCalloutRelationship",
            nullptr,
            {
                {"Name"},
                {"Description"},
                {"RelatingDraughtingCallout"},
                {"RelatedDraughtingCallout"},
                
            }
        );
        schema.define(
            "IfcEnvironmentalImpactValue",
            IfcAppliedValue,
            {
                {"ImpactType"},
                {"Category"},
                {"UserDefinedCategory"},
                
            }
        );
        CoreClass* IfcExternalReference = schema.define(
            "IfcExternalReference",
            nullptr,
            {
                {"Location"},
                {"ItemReference"},
                {"Name"},
                
            }
        );
        schema.define(
            "IfcExternallyDefinedHatchStyle",
            IfcExternalReference,
            {
                
            }
        );
        schema.define(
            "IfcExternallyDefinedSurfaceStyle",
            IfcExternalReference,
            {
                
            }
        );
        schema.define(
            "IfcExternallyDefinedSymbol",
            IfcExternalReference,
            {
                
            }
        );
        schema.define(
            "IfcExternallyDefinedTextFont",
            IfcExternalReference,
            {
                
            }
        );
        schema.define(
            "IfcGridAxis",
            nullptr,
            {
                {"AxisTag"},
                {"AxisCurve"},
                {"SameSense"},
                
            }
        );
        schema.define(
            "IfcIrregularTimeSeriesValue",
            nullptr,
            {
                {"TimeStamp"},
                {"ListValues"},
                
            }
        );
        schema.define(
            "IfcLibraryInformation",
            nullptr,
            {
                {"Name"},
                {"Version"},
                {"Publisher"},
                {"VersionDate"},
                {"LibraryReference"},
                
            }
        );
        schema.define(
            "IfcLibraryReference",
            IfcExternalReference,
            {
                
            }
        );
        schema.define(
            "IfcLightDistributionData",
            nullptr,
            {
                {"MainPlaneAngle"},
                {"SecondaryPlaneAngle"},
                {"LuminousIntensity"},
                
            }
        );
        schema.define(
            "IfcLightIntensityDistribution",
            nullptr,
            {
                {"LightDistributionCurve"},
                {"DistributionData"},
                
            }
        );
        schema.define(
            "IfcLocalTime",
            nullptr,
            {
                {"HourComponent"},
                {"MinuteComponent"},
                {"SecondComponent"},
                {"Zone"},
                {"DaylightSavingOffset"},
                
            }
        );
        schema.define(
            "IfcMaterial",
            nullptr,
            {
                {"Name"},
                
            }
        );
        schema.define(
            "IfcMaterialClassificationRelationship",
            nullptr,
            {
                {"MaterialClassifications"},
                {"ClassifiedMaterial"},
                
            }
        );
        schema.define(
            "IfcMaterialLayer",
            nullptr,
            {
                {"Material"},
                {"LayerThickness"},
                {"IsVentilated"},
                
            }
        );
        schema.define(
            "IfcMaterialLayerSet",
            nullptr,
            {
                {"MaterialLayers"},
                {"LayerSetName"},
                
            }
        );
        schema.define(
            "IfcMaterialLayerSetUsage",
            nullptr,
            {
                {"ForLayerSet"},
                {"LayerSetDirection"},
                {"DirectionSense"},
                {"OffsetFromReferenceLine"},
                
            }
        );
        schema.define(
            "IfcMaterialList",
            nullptr,
            {
                {"Materials"},
                
            }
        );
        CoreClass* IfcMaterialProperties = schema.define(
            "IfcMaterialProperties",
            nullptr,
            {
                {"Material"},
                
            }
        );
        schema.define(
            "IfcMeasureWithUnit",
            nullptr,
            {
                {"ValueComponent"},
                {"UnitComponent"},
                
            }
        );
        CoreClass* IfcMechanicalMaterialProperties = schema.define(
            "IfcMechanicalMaterialProperties",
            IfcMaterialProperties,
            {
                {"DynamicViscosity"},
                {"YoungModulus"},
                {"ShearModulus"},
                {"PoissonRatio"},
                {"ThermalExpansionCoefficient"},
                
            }
        );
        schema.define(
            "IfcMechanicalSteelMaterialProperties",
            IfcMechanicalMaterialProperties,
            {
                {"YieldStress"},
                {"UltimateStress"},
                {"UltimateStrain"},
                {"HardeningModule"},
                {"ProportionalStress"},
                {"PlasticStrain"},
                {"Relaxations"},
                
            }
        );
        schema.define(
            "IfcMetric",
            IfcConstraint,
            {
                {"Benchmark"},
                {"ValueSource"},
                {"DataValue"},
                
            }
        );
        schema.define(
            "IfcMonetaryUnit",
            nullptr,
            {
                {"Currency"},
                
            }
        );
        CoreClass* IfcNamedUnit = schema.define(
            "IfcNamedUnit",
            nullptr,
            {
                {"Dimensions"},
                {"UnitType"},
                
            }
        );
        CoreClass* IfcObjectPlacement = schema.define(
            "IfcObjectPlacement",
            nullptr,
            {
                
            }
        );
        schema.define(
            "IfcObjective",
            IfcConstraint,
            {
                {"BenchmarkValues"},
                {"ResultValues"},
                {"ObjectiveQualifier"},
                {"UserDefinedQualifier"},
                
            }
        );
        schema.define(
            "IfcOpticalMaterialProperties",
            IfcMaterialProperties,
            {
                {"VisibleTransmittance"},
                {"SolarTransmittance"},
                {"ThermalIrTransmittance"},
                {"ThermalIrEmissivityBack"},
                {"ThermalIrEmissivityFront"},
                {"VisibleReflectanceBack"},
                {"VisibleReflectanceFront"},
                {"SolarReflectanceFront"},
                {"SolarReflectanceBack"},
                
            }
        );
        schema.define(
            "IfcOrganization",
            nullptr,
            {
                {"Id"},
                {"Name"},
                {"Description"},
                {"Roles"},
                {"Addresses"},
                
            }
        );
        schema.define(
            "IfcOrganizationRelationship",
            nullptr,
            {
                {"Name"},
                {"Description"},
                {"RelatingOrganization"},
                {"RelatedOrganizations"},
                
            }
        );
        schema.define(
            "IfcOwnerHistory",
            nullptr,
            {
                {"OwningUser"},
                {"OwningApplication"},
                {"State"},
                {"ChangeAction"},
                {"LastModifiedDate"},
                {"LastModifyingUser"},
                {"LastModifyingApplication"},
                {"CreationDate"},
                
            }
        );
        schema.define(
            "IfcPerson",
            nullptr,
            {
                {"Id"},
                {"FamilyName"},
                {"GivenName"},
                {"MiddleNames"},
                {"PrefixTitles"},
                {"SuffixTitles"},
                {"Roles"},
                {"Addresses"},
                
            }
        );
        schema.define(
            "IfcPersonAndOrganization",
            nullptr,
            {
                {"ThePerson"},
                {"TheOrganization"},
                {"Roles"},
                
            }
        );
        CoreClass* IfcPhysicalQuantity = schema.define(
            "IfcPhysicalQuantity",
            nullptr,
            {
                {"Name"},
                {"Description"},
                
            }
        );
        CoreClass* IfcPhysicalSimpleQuantity = schema.define(
            "IfcPhysicalSimpleQuantity",
            IfcPhysicalQuantity,
            {
                {"Unit"},
                
            }
        );
        schema.define(
            "IfcPostalAddress",
            IfcAddress,
            {
                {"InternalLocation"},
                {"AddressLines"},
                {"PostalBox"},
                {"Town"},
                {"Region"},
                {"PostalCode"},
                {"Country"},
                
            }
        );
        CoreClass* IfcPreDefinedItem = schema.define(
            "IfcPreDefinedItem",
            nullptr,
            {
                {"Name"},
                
            }
        );
        CoreClass* IfcPreDefinedSymbol = schema.define(
            "IfcPreDefinedSymbol",
            IfcPreDefinedItem,
            {
                
            }
        );
        schema.define(
            "IfcPreDefinedTerminatorSymbol",
            IfcPreDefinedSymbol,
            {
                
            }
        );
        CoreClass* IfcPreDefinedTextFont = schema.define(
            "IfcPreDefinedTextFont",
            IfcPreDefinedItem,
            {
                
            }
        );
        CoreClass* IfcPresentationLayerAssignment = schema.define(
            "IfcPresentationLayerAssignment",
            nullptr,
            {
                {"Name"},
                {"Description"},
                {"AssignedItems"},
                {"Identifier"},
                
            }
        );
        schema.define(
            "IfcPresentationLayerWithStyle",
            IfcPresentationLayerAssignment,
            {
                {"LayerOn"},
                {"LayerFrozen"},
                {"LayerBlocked"},
                {"LayerStyles"},
                
            }
        );
        CoreClass* IfcPresentationStyle = schema.define(
            "IfcPresentationStyle",
            nullptr,
            {
                {"Name"},
                
            }
        );
        schema.define(
            "IfcPresentationStyleAssignment",
            nullptr,
            {
                {"Styles"},
                
            }
        );
        CoreClass* IfcProductRepresentation = schema.define(
            "IfcProductRepresentation",
            nullptr,
            {
                {"Name"},
                {"Description"},
                {"Representations"},
                
            }
        );
        schema.define(
            "IfcProductsOfCombustionProperties",
            IfcMaterialProperties,
            {
                {"SpecificHeatCapacity"},
                {"N20Content"},
                {"COContent"},
                {"CO2Content"},
                
            }
        );
        CoreClass* IfcProfileDef = schema.define(
            "IfcProfileDef",
            nullptr,
            {
                {"ProfileType"},
                {"ProfileName"},
                
            }
        );
        CoreClass* IfcProfileProperties = schema.define(
            "IfcProfileProperties",
            nullptr,
            {
                {"ProfileName"},
                {"ProfileDefinition"},
                
            }
        );
        CoreClass* IfcProperty = schema.define(
            "IfcProperty",
            nullptr,
            {
                {"Name"},
                {"Description"},
                
            }
        );
        schema.define(
            "IfcPropertyConstraintRelationship",
            nullptr,
            {
                {"RelatingConstraint"},
                {"RelatedProperties"},
                {"Name"},
                {"Description"},
                
            }
        );
        schema.define(
            "IfcPropertyDependencyRelationship",
            nullptr,
            {
                {"DependingProperty"},
                {"DependantProperty"},
                {"Name"},
                {"Description"},
                {"Expression"},
                
            }
        );
        schema.define(
            "IfcPropertyEnumeration",
            nullptr,
            {
                {"Name"},
                {"EnumerationValues"},
                {"Unit"},
                
            }
        );
        schema.define(
            "IfcQuantityArea",
            IfcPhysicalSimpleQuantity,
            {
                {"AreaValue"},
                
            }
        );
        schema.define(
            "IfcQuantityCount",
            IfcPhysicalSimpleQuantity,
            {
                {"CountValue"},
                
            }
        );
        schema.define(
            "IfcQuantityLength",
            IfcPhysicalSimpleQuantity,
            {
                {"LengthValue"},
                
            }
        );
        schema.define(
            "IfcQuantityTime",
            IfcPhysicalSimpleQuantity,
            {
                {"TimeValue"},
                
            }
        );
        schema.define(
            "IfcQuantityVolume",
            IfcPhysicalSimpleQuantity,
            {
                {"VolumeValue"},
                
            }
        );
        schema.define(
            "IfcQuantityWeight",
            IfcPhysicalSimpleQuantity,
            {
                {"WeightValue"},
                
            }
        );
        schema.define(
            "IfcReferencesValueDocument",
            nullptr,
            {
                {"ReferencedDocument"},
                {"ReferencingValues"},
                {"Name"},
                {"Description"},
                
            }
        );
        schema.define(
            "IfcReinforcementBarProperties",
            nullptr,
            {
                {"TotalCrossSectionArea"},
                {"SteelGrade"},
                {"BarSurface"},
                {"EffectiveDepth"},
                {"NominalBarDiameter"},
                {"BarCount"},
                
            }
        );
        schema.define(
            "IfcRelaxation",
            nullptr,
            {
                {"RelaxationValue"},
                {"InitialStress"},
                
            }
        );
        CoreClass* IfcRepresentation = schema.define(
            "IfcRepresentation",
            nullptr,
            {
                {"ContextOfItems"},
                {"RepresentationIdentifier"},
                {"RepresentationType"},
                {"Items"},
                
            }
        );
        CoreClass* IfcRepresentationContext = schema.define(
            "IfcRepresentationContext",
            nullptr,
            {
                {"ContextIdentifier"},
                {"ContextType"},
                
            }
        );
        CoreClass* IfcRepresentationItem = schema.define(
            "IfcRepresentationItem",
            nullptr,
            {
                
            }
        );
        schema.define(
            "IfcRepresentationMap",
            nullptr,
            {
                {"MappingOrigin"},
                {"MappedRepresentation"},
                
            }
        );
        schema.define(
            "IfcRibPlateProfileProperties",
            IfcProfileProperties,
            {
                {"Thickness"},
                {"RibHeight"},
                {"RibWidth"},
                {"RibSpacing"},
                {"Direction"},
                
            }
        );
        CoreClass* IfcRoot = schema.define(
            "IfcRoot",
            nullptr,
            {
                {"GlobalId"},
                {"OwnerHistory"},
                {"Name"},
                {"Description"},
                
            }
        );
        schema.define(
            "IfcSIUnit",
            IfcNamedUnit,
            {
                {"Prefix"},
                {"Name"},
                
            }
        );
        schema.define(
            "IfcSectionProperties",
            nullptr,
            {
                {"SectionType"},
                {"StartProfile"},
                {"EndProfile"},
                
            }
        );
        schema.define(
            "IfcSectionReinforcementProperties",
            nullptr,
            {
                {"LongitudinalStartPosition"},
                {"LongitudinalEndPosition"},
                {"TransversePosition"},
                {"ReinforcementRole"},
                {"SectionDefinition"},
                {"CrossSectionReinforcementDefinitions"},
                
            }
        );
        schema.define(
            "IfcShapeAspect",
            nullptr,
            {
                {"ShapeRepresentations"},
                {"Name"},
                {"Description"},
                {"ProductDefinitional"},
                {"PartOfProductDefinitionShape"},
                
            }
        );
        CoreClass* IfcShapeModel = schema.define(
            "IfcShapeModel",
            IfcRepresentation,
            {
                
            }
        );
        schema.define(
            "IfcShapeRepresentation",
            IfcShapeModel,
            {
                
            }
        );
        CoreClass* IfcSimpleProperty = schema.define(
            "IfcSimpleProperty",
            IfcProperty,
            {
                
            }
        );
        CoreClass* IfcStructuralConnectionCondition = schema.define(
            "IfcStructuralConnectionCondition",
            nullptr,
            {
                {"Name"},
                
            }
        );
        CoreClass* IfcStructuralLoad = schema.define(
            "IfcStructuralLoad",
            nullptr,
            {
                {"Name"},
                
            }
        );
        CoreClass* IfcStructuralLoadStatic = schema.define(
            "IfcStructuralLoadStatic",
            IfcStructuralLoad,
            {
                
            }
        );
        schema.define(
            "IfcStructuralLoadTemperature",
            IfcStructuralLoadStatic,
            {
                {"DeltaT_Constant"},
                {"DeltaT_Y"},
                {"DeltaT_Z"},
                
            }
        );
        CoreClass* IfcStyleModel = schema.define(
            "IfcStyleModel",
            IfcRepresentation,
            {
                
            }
        );
        CoreClass* IfcStyledItem = schema.define(
            "IfcStyledItem",
            IfcRepresentationItem,
            {
                {"Item"},
                {"Styles"},
                {"Name"},
                
            }
        );
        schema.define(
            "IfcStyledRepresentation",
            IfcStyleModel,
            {
                
            }
        );
        schema.define(
            "IfcSurfaceStyle",
            IfcPresentationStyle,
            {
                {"Side"},
                {"Styles"},
                
            }
        );
        schema.define(
            "IfcSurfaceStyleLighting",
            nullptr,
            {
                {"DiffuseTransmissionColour"},
                {"DiffuseReflectionColour"},
                {"TransmissionColour"},
                {"ReflectanceColour"},
                
            }
        );
        schema.define(
            "IfcSurfaceStyleRefraction",
            nullptr,
            {
                {"RefractionIndex"},
                {"DispersionFactor"},
                
            }
        );
        CoreClass* IfcSurfaceStyleShading = schema.define(
            "IfcSurfaceStyleShading",
            nullptr,
            {
                {"SurfaceColour"},
                
            }
        );
        schema.define(
            "IfcSurfaceStyleWithTextures",
            nullptr,
            {
                {"Textures"},
                
            }
        );
        CoreClass* IfcSurfaceTexture = schema.define(
            "IfcSurfaceTexture",
            nullptr,
            {
                {"RepeatS"},
                {"RepeatT"},
                {"TextureType"},
                {"TextureTransform"},
                
            }
        );
        schema.define(
            "IfcSymbolStyle",
            IfcPresentationStyle,
            {
                {"StyleOfSymbol"},
                
            }
        );
        schema.define(
            "IfcTable",
            nullptr,
            {
                {"Name"},
                {"Rows"},
                
            }
        );
        schema.define(
            "IfcTableRow",
            nullptr,
            {
                {"RowCells"},
                {"IsHeading"},
                
            }
        );
        schema.define(
            "IfcTelecomAddress",
            IfcAddress,
            {
                {"TelephoneNumbers"},
                {"FacsimileNumbers"},
                {"PagerNumber"},
                {"ElectronicMailAddresses"},
                {"WWWHomePageURL"},
                
            }
        );
        schema.define(
            "IfcTextStyle",
            IfcPresentationStyle,
            {
                {"TextCharacterAppearance"},
                {"TextStyle"},
                {"TextFontStyle"},
                
            }
        );
        schema.define(
            "IfcTextStyleFontModel",
            IfcPreDefinedTextFont,
            {
                {"FontFamily"},
                {"FontStyle"},
                {"FontVariant"},
                {"FontWeight"},
                {"FontSize"},
                
            }
        );
        schema.define(
            "IfcTextStyleForDefinedFont",
            nullptr,
            {
                {"Colour"},
                {"BackgroundColour"},
                
            }
        );
        schema.define(
            "IfcTextStyleTextModel",
            nullptr,
            {
                {"TextIndent"},
                {"TextAlign"},
                {"TextDecoration"},
                {"LetterSpacing"},
                {"WordSpacing"},
                {"TextTransform"},
                {"LineHeight"},
                
            }
        );
        schema.define(
            "IfcTextStyleWithBoxCharacteristics",
            nullptr,
            {
                {"BoxHeight"},
                {"BoxWidth"},
                {"BoxSlantAngle"},
                {"BoxRotateAngle"},
                {"CharacterSpacing"},
                
            }
        );
        CoreClass* IfcTextureCoordinate = schema.define(
            "IfcTextureCoordinate",
            nullptr,
            {
                
            }
        );
        schema.define(
            "IfcTextureCoordinateGenerator",
            IfcTextureCoordinate,
            {
                {"Mode"},
                {"Parameter"},
                
            }
        );
        schema.define(
            "IfcTextureMap",
            IfcTextureCoordinate,
            {
                {"TextureMaps"},
                
            }
        );
        schema.define(
            "IfcTextureVertex",
            nullptr,
            {
                {"Coordinates"},
                
            }
        );
        schema.define(
            "IfcThermalMaterialProperties",
            IfcMaterialProperties,
            {
                {"SpecificHeatCapacity"},
                {"BoilingPoint"},
                {"FreezingPoint"},
                {"ThermalConductivity"},
                
            }
        );
        CoreClass* IfcTimeSeries = schema.define(
            "IfcTimeSeries",
            nullptr,
            {
                {"Name"},
                {"Description"},
                {"StartTime"},
                {"EndTime"},
                {"TimeSeriesDataType"},
                {"DataOrigin"},
                {"UserDefinedDataOrigin"},
                {"Unit"},
                
            }
        );
        schema.define(
            "IfcTimeSeriesReferenceRelationship",
            nullptr,
            {
                {"ReferencedTimeSeries"},
                {"TimeSeriesReferences"},
                
            }
        );
        schema.define(
            "IfcTimeSeriesValue",
            nullptr,
            {
                {"ListValues"},
                
            }
        );
        CoreClass* IfcTopologicalRepresentationItem = schema.define(
            "IfcTopologicalRepresentationItem",
            IfcRepresentationItem,
            {
                
            }
        );
        schema.define(
            "IfcTopologyRepresentation",
            IfcShapeModel,
            {
                
            }
        );
        schema.define(
            "IfcUnitAssignment",
            nullptr,
            {
                {"Units"},
                
            }
        );
        CoreClass* IfcVertex = schema.define(
            "IfcVertex",
            IfcTopologicalRepresentationItem,
            {
                
            }
        );
        schema.define(
            "IfcVertexBasedTextureMap",
            nullptr,
            {
                {"TextureVertices"},
                {"TexturePoints"},
                
            }
        );
        schema.define(
            "IfcVertexPoint",
            IfcVertex,
            {
                {"VertexGeometry"},
                
            }
        );
        schema.define(
            "IfcVirtualGridIntersection",
            nullptr,
            {
                {"IntersectingAxes"},
                {"OffsetDistances"},
                
            }
        );
        schema.define(
            "IfcWaterProperties",
            IfcMaterialProperties,
            {
                {"IsPotable"},
                {"Hardness"},
                {"AlkalinityConcentration"},
                {"AcidityConcentration"},
                {"ImpuritiesContent"},
                {"PHLevel"},
                {"DissolvedSolidsContent"},
                
            }
        );
        CoreClass* IfcAnnotationOccurrence = schema.define(
            "IfcAnnotationOccurrence",
            IfcStyledItem,
            {
                
            }
        );
        schema.define(
            "IfcAnnotationSurfaceOccurrence",
            IfcAnnotationOccurrence,
            {
                
            }
        );
        CoreClass* IfcAnnotationSymbolOccurrence = schema.define(
            "IfcAnnotationSymbolOccurrence",
            IfcAnnotationOccurrence,
            {
                
            }
        );
        schema.define(
            "IfcAnnotationTextOccurrence",
            IfcAnnotationOccurrence,
            {
                
            }
        );
        CoreClass* IfcArbitraryClosedProfileDef = schema.define(
            "IfcArbitraryClosedProfileDef",
            IfcProfileDef,
            {
                {"OuterCurve"},
                
            }
        );
        CoreClass* IfcArbitraryOpenProfileDef = schema.define(
            "IfcArbitraryOpenProfileDef",
            IfcProfileDef,
            {
                {"Curve"},
                
            }
        );
        schema.define(
            "IfcArbitraryProfileDefWithVoids",
            IfcArbitraryClosedProfileDef,
            {
                {"InnerCurves"},
                
            }
        );
        schema.define(
            "IfcBlobTexture",
            IfcSurfaceTexture,
            {
                {"RasterFormat"},
                {"RasterCode"},
                
            }
        );
        schema.define(
            "IfcCenterLineProfileDef",
            IfcArbitraryOpenProfileDef,
            {
                {"Thickness"},
                
            }
        );
        schema.define(
            "IfcClassificationReference",
            IfcExternalReference,
            {
                {"ReferencedSource"},
                
            }
        );
        schema.define(
            "IfcColourRgb",
            IfcColourSpecification,
            {
                {"Red"},
                {"Green"},
                {"Blue"},
                
            }
        );
        schema.define(
            "IfcComplexProperty",
            IfcProperty,
            {
                {"UsageName"},
                {"HasProperties"},
                
            }
        );
        schema.define(
            "IfcCompositeProfileDef",
            IfcProfileDef,
            {
                {"Profiles"},
                {"Label"},
                
            }
        );
        CoreClass* IfcConnectedFaceSet = schema.define(
            "IfcConnectedFaceSet",
            IfcTopologicalRepresentationItem,
            {
                {"CfsFaces"},
                
            }
        );
        schema.define(
            "IfcConnectionCurveGeometry",
            IfcConnectionGeometry,
            {
                {"CurveOnRelatingElement"},
                {"CurveOnRelatedElement"},
                
            }
        );
        schema.define(
            "IfcConnectionPointEccentricity",
            IfcConnectionPointGeometry,
            {
                {"EccentricityInX"},
                {"EccentricityInY"},
                {"EccentricityInZ"},
                
            }
        );
        schema.define(
            "IfcContextDependentUnit",
            IfcNamedUnit,
            {
                {"Name"},
                
            }
        );
        schema.define(
            "IfcConversionBasedUnit",
            IfcNamedUnit,
            {
                {"Name"},
                {"ConversionFactor"},
                
            }
        );
        schema.define(
            "IfcCurveStyle",
            IfcPresentationStyle,
            {
                {"CurveFont"},
                {"CurveWidth"},
                {"CurveColour"},
                
            }
        );
        schema.define(
            "IfcDerivedProfileDef",
            IfcProfileDef,
            {
                {"ParentProfile"},
                {"Operator"},
                {"Label"},
                
            }
        );
        schema.define(
            "IfcDimensionCalloutRelationship",
            IfcDraughtingCalloutRelationship,
            {
                
            }
        );
        schema.define(
            "IfcDimensionPair",
            IfcDraughtingCalloutRelationship,
            {
                
            }
        );
        schema.define(
            "IfcDocumentReference",
            IfcExternalReference,
            {
                
            }
        );
        schema.define(
            "IfcDraughtingPreDefinedTextFont",
            IfcPreDefinedTextFont,
            {
                
            }
        );
        CoreClass* IfcEdge = schema.define(
            "IfcEdge",
            IfcTopologicalRepresentationItem,
            {
                {"EdgeStart"},
                {"EdgeEnd"},
                
            }
        );
        schema.define(
            "IfcEdgeCurve",
            IfcEdge,
            {
                {"EdgeGeometry"},
                {"SameSense"},
                
            }
        );
        schema.define(
            "IfcExtendedMaterialProperties",
            IfcMaterialProperties,
            {
                {"ExtendedProperties"},
                {"Description"},
                {"Name"},
                
            }
        );
        CoreClass* IfcFace = schema.define(
            "IfcFace",
            IfcTopologicalRepresentationItem,
            {
                {"Bounds"},
                
            }
        );
        CoreClass* IfcFaceBound = schema.define(
            "IfcFaceBound",
            IfcTopologicalRepresentationItem,
            {
                {"Bound"},
                {"Orientation"},
                
            }
        );
        schema.define(
            "IfcFaceOuterBound",
            IfcFaceBound,
            {
                
            }
        );
        schema.define(
            "IfcFaceSurface",
            IfcFace,
            {
                {"FaceSurface"},
                {"SameSense"},
                
            }
        );
        schema.define(
            "IfcFailureConnectionCondition",
            IfcStructuralConnectionCondition,
            {
                {"TensionFailureX"},
                {"TensionFailureY"},
                {"TensionFailureZ"},
                {"CompressionFailureX"},
                {"CompressionFailureY"},
                {"CompressionFailureZ"},
                
            }
        );
        schema.define(
            "IfcFillAreaStyle",
            IfcPresentationStyle,
            {
                {"FillStyles"},
                
            }
        );
        schema.define(
            "IfcFuelProperties",
            IfcMaterialProperties,
            {
                {"CombustionTemperature"},
                {"CarbonContent"},
                {"LowerHeatingValue"},
                {"HigherHeatingValue"},
                
            }
        );
        schema.define(
            "IfcGeneralMaterialProperties",
            IfcMaterialProperties,
            {
                {"MolecularWeight"},
                {"Porosity"},
                {"MassDensity"},
                
            }
        );
        CoreClass* IfcGeneralProfileProperties = schema.define(
            "IfcGeneralProfileProperties",
            IfcProfileProperties,
            {
                {"PhysicalWeight"},
                {"Perimeter"},
                {"MinimumPlateThickness"},
                {"MaximumPlateThickness"},
                {"CrossSectionArea"},
                
            }
        );
        CoreClass* IfcGeometricRepresentationContext = schema.define(
            "IfcGeometricRepresentationContext",
            IfcRepresentationContext,
            {
                {"CoordinateSpaceDimension"},
                {"Precision"},
                {"WorldCoordinateSystem"},
                {"TrueNorth"},
                
            }
        );
        CoreClass* IfcGeometricRepresentationItem = schema.define(
            "IfcGeometricRepresentationItem",
            IfcRepresentationItem,
            {
                
            }
        );
        schema.define(
            "IfcGeometricRepresentationSubContext",
            IfcGeometricRepresentationContext,
            {
                {"ParentContext"},
                {"TargetScale"},
                {"TargetView"},
                {"UserDefinedTargetView"},
                
            }
        );
        CoreClass* IfcGeometricSet = schema.define(
            "IfcGeometricSet",
            IfcGeometricRepresentationItem,
            {
                {"Elements"},
                
            }
        );
        schema.define(
            "IfcGridPlacement",
            IfcObjectPlacement,
            {
                {"PlacementLocation"},
                {"PlacementRefDirection"},
                
            }
        );
        CoreClass* IfcHalfSpaceSolid = schema.define(
            "IfcHalfSpaceSolid",
            IfcGeometricRepresentationItem,
            {
                {"BaseSurface"},
                {"AgreementFlag"},
                
            }
        );
        schema.define(
            "IfcHygroscopicMaterialProperties",
            IfcMaterialProperties,
            {
                {"UpperVaporResistanceFactor"},
                {"LowerVaporResistanceFactor"},
                {"IsothermalMoistureCapacity"},
                {"VaporPermeability"},
                {"MoistureDiffusivity"},
                
            }
        );
        schema.define(
            "IfcImageTexture",
            IfcSurfaceTexture,
            {
                {"UrlReference"},
                
            }
        );
        schema.define(
            "IfcIrregularTimeSeries",
            IfcTimeSeries,
            {
                {"Values"},
                
            }
        );
        CoreClass* IfcLightSource = schema.define(
            "IfcLightSource",
            IfcGeometricRepresentationItem,
            {
                {"Name"},
                {"LightColour"},
                {"AmbientIntensity"},
                {"Intensity"},
                
            }
        );
        schema.define(
            "IfcLightSourceAmbient",
            IfcLightSource,
            {
                
            }
        );
        schema.define(
            "IfcLightSourceDirectional",
            IfcLightSource,
            {
                {"Orientation"},
                
            }
        );
        schema.define(
            "IfcLightSourceGoniometric",
            IfcLightSource,
            {
                {"Position"},
                {"ColourAppearance"},
                {"ColourTemperature"},
                {"LuminousFlux"},
                {"LightEmissionSource"},
                {"LightDistributionDataSource"},
                
            }
        );
        CoreClass* IfcLightSourcePositional = schema.define(
            "IfcLightSourcePositional",
            IfcLightSource,
            {
                {"Position"},
                {"Radius"},
                {"ConstantAttenuation"},
                {"DistanceAttenuation"},
                {"QuadricAttenuation"},
                
            }
        );
        schema.define(
            "IfcLightSourceSpot",
            IfcLightSourcePositional,
            {
                {"Orientation"},
                {"ConcentrationExponent"},
                {"SpreadAngle"},
                {"BeamWidthAngle"},
                
            }
        );
        schema.define(
            "IfcLocalPlacement",
            IfcObjectPlacement,
            {
                {"PlacementRelTo"},
                {"RelativePlacement"},
                
            }
        );
        CoreClass* IfcLoop = schema.define(
            "IfcLoop",
            IfcTopologicalRepresentationItem,
            {
                
            }
        );
        schema.define(
            "IfcMappedItem",
            IfcRepresentationItem,
            {
                {"MappingSource"},
                {"MappingTarget"},
                
            }
        );
        schema.define(
            "IfcMaterialDefinitionRepresentation",
            IfcProductRepresentation,
            {
                {"RepresentedMaterial"},
                
            }
        );
        schema.define(
            "IfcMechanicalConcreteMaterialProperties",
            IfcMechanicalMaterialProperties,
            {
                {"CompressiveStrength"},
                {"MaxAggregateSize"},
                {"AdmixturesDescription"},
                {"Workability"},
                {"ProtectivePoreRatio"},
                {"WaterImpermeability"},
                
            }
        );
        CoreClass* IfcObjectDefinition = schema.define(
            "IfcObjectDefinition",
            IfcRoot,
            {
                
            }
        );
        CoreClass* IfcOneDirectionRepeatFactor = schema.define(
            "IfcOneDirectionRepeatFactor",
            IfcGeometricRepresentationItem,
            {
                {"RepeatFactor"},
                
            }
        );
        schema.define(
            "IfcOpenShell",
            IfcConnectedFaceSet,
            {
                
            }
        );
        schema.define(
            "IfcOrientedEdge",
            IfcEdge,
            {
                {"EdgeElement"},
                {"Orientation"},
                
            }
        );
        CoreClass* IfcParameterizedProfileDef = schema.define(
            "IfcParameterizedProfileDef",
            IfcProfileDef,
            {
                {"Position"},
                
            }
        );
        schema.define(
            "IfcPath",
            IfcTopologicalRepresentationItem,
            {
                {"EdgeList"},
                
            }
        );
        schema.define(
            "IfcPhysicalComplexQuantity",
            IfcPhysicalQuantity,
            {
                {"HasQuantities"},
                {"Discrimination"},
                {"Quality"},
                {"Usage"},
                
            }
        );
        schema.define(
            "IfcPixelTexture",
            IfcSurfaceTexture,
            {
                {"Width"},
                {"Height"},
                {"ColourComponents"},
                {"Pixel"},
                
            }
        );
        CoreClass* IfcPlacement = schema.define(
            "IfcPlacement",
            IfcGeometricRepresentationItem,
            {
                {"Location"},
                
            }
        );
        CoreClass* IfcPlanarExtent = schema.define(
            "IfcPlanarExtent",
            IfcGeometricRepresentationItem,
            {
                {"SizeInX"},
                {"SizeInY"},
                
            }
        );
        CoreClass* IfcPoint = schema.define(
            "IfcPoint",
            IfcGeometricRepresentationItem,
            {
                
            }
        );
        schema.define(
            "IfcPointOnCurve",
            IfcPoint,
            {
                {"BasisCurve"},
                {"PointParameter"},
                
            }
        );
        schema.define(
            "IfcPointOnSurface",
            IfcPoint,
            {
                {"BasisSurface"},
                {"PointParameterU"},
                {"PointParameterV"},
                
            }
        );
        schema.define(
            "IfcPolyLoop",
            IfcLoop,
            {
                {"Polygon"},
                
            }
        );
        schema.define(
            "IfcPolygonalBoundedHalfSpace",
            IfcHalfSpaceSolid,
            {
                {"Position"},
                {"PolygonalBoundary"},
                
            }
        );
        CoreClass* IfcPreDefinedColour = schema.define(
            "IfcPreDefinedColour",
            IfcPreDefinedItem,
            {
                
            }
        );
        CoreClass* IfcPreDefinedCurveFont = schema.define(
            "IfcPreDefinedCurveFont",
            IfcPreDefinedItem,
            {
                
            }
        );
        schema.define(
            "IfcPreDefinedDimensionSymbol",
            IfcPreDefinedSymbol,
            {
                
            }
        );
        schema.define(
            "IfcPreDefinedPointMarkerSymbol",
            IfcPreDefinedSymbol,
            {
                
            }
        );
        schema.define(
            "IfcProductDefinitionShape",
            IfcProductRepresentation,
            {
                
            }
        );
        schema.define(
            "IfcPropertyBoundedValue",
            IfcSimpleProperty,
            {
                {"UpperBoundValue"},
                {"LowerBoundValue"},
                {"Unit"},
                
            }
        );
        CoreClass* IfcPropertyDefinition = schema.define(
            "IfcPropertyDefinition",
            IfcRoot,
            {
                
            }
        );
        schema.define(
            "IfcPropertyEnumeratedValue",
            IfcSimpleProperty,
            {
                {"EnumerationValues"},
                {"EnumerationReference"},
                
            }
        );
        schema.define(
            "IfcPropertyListValue",
            IfcSimpleProperty,
            {
                {"ListValues"},
                {"Unit"},
                
            }
        );
        schema.define(
            "IfcPropertyReferenceValue",
            IfcSimpleProperty,
            {
                {"UsageName"},
                {"PropertyReference"},
                
            }
        );
        CoreClass* IfcPropertySetDefinition = schema.define(
            "IfcPropertySetDefinition",
            IfcPropertyDefinition,
            {
                
            }
        );
        schema.define(
            "IfcPropertySingleValue",
            IfcSimpleProperty,
            {
                {"NominalValue"},
                {"Unit"},
                
            }
        );
        schema.define(
            "IfcPropertyTableValue",
            IfcSimpleProperty,
            {
                {"DefiningValues"},
                {"DefinedValues"},
                {"Expression"},
                {"DefiningUnit"},
                {"DefinedUnit"},
                
            }
        );
        CoreClass* IfcRectangleProfileDef = schema.define(
            "IfcRectangleProfileDef",
            IfcParameterizedProfileDef,
            {
                {"XDim"},
                {"YDim"},
                
            }
        );
        schema.define(
            "IfcRegularTimeSeries",
            IfcTimeSeries,
            {
                {"TimeStep"},
                {"Values"},
                
            }
        );
        schema.define(
            "IfcReinforcementDefinitionProperties",
            IfcPropertySetDefinition,
            {
                {"DefinitionType"},
                {"ReinforcementSectionDefinitions"},
                
            }
        );
        CoreClass* IfcRelationship = schema.define(
            "IfcRelationship",
            IfcRoot,
            {
                
            }
        );
        schema.define(
            "IfcRoundedRectangleProfileDef",
            IfcRectangleProfileDef,
            {
                {"RoundingRadius"},
                
            }
        );
        schema.define(
            "IfcSectionedSpine",
            IfcGeometricRepresentationItem,
            {
                {"SpineCurve"},
                {"CrossSections"},
                {"CrossSectionPositions"},
                
            }
        );
        schema.define(
            "IfcServiceLifeFactor",
            IfcPropertySetDefinition,
            {
                {"PredefinedType"},
                {"UpperValue"},
                {"MostUsedValue"},
                {"LowerValue"},
                
            }
        );
        schema.define(
            "IfcShellBasedSurfaceModel",
            IfcGeometricRepresentationItem,
            {
                {"SbsmBoundary"},
                
            }
        );
        schema.define(
            "IfcSlippageConnectionCondition",
            IfcStructuralConnectionCondition,
            {
                {"SlippageX"},
                {"SlippageY"},
                {"SlippageZ"},
                
            }
        );
        CoreClass* IfcSolidModel = schema.define(
            "IfcSolidModel",
            IfcGeometricRepresentationItem,
            {
                
            }
        );
        schema.define(
            "IfcSoundProperties",
            IfcPropertySetDefinition,
            {
                {"IsAttenuating"},
                {"SoundScale"},
                {"SoundValues"},
                
            }
        );
        schema.define(
            "IfcSoundValue",
            IfcPropertySetDefinition,
            {
                {"SoundLevelTimeSeries"},
                {"Frequency"},
                {"SoundLevelSingleValue"},
                
            }
        );
        schema.define(
            "IfcSpaceThermalLoadProperties",
            IfcPropertySetDefinition,
            {
                {"ApplicableValueRatio"},
                {"ThermalLoadSource"},
                {"PropertySource"},
                {"SourceDescription"},
                {"MaximumValue"},
                {"MinimumValue"},
                {"ThermalLoadTimeSeriesValues"},
                {"UserDefinedThermalLoadSource"},
                {"UserDefinedPropertySource"},
                {"ThermalLoadType"},
                
            }
        );
        schema.define(
            "IfcStructuralLoadLinearForce",
            IfcStructuralLoadStatic,
            {
                {"LinearForceX"},
                {"LinearForceY"},
                {"LinearForceZ"},
                {"LinearMomentX"},
                {"LinearMomentY"},
                {"LinearMomentZ"},
                
            }
        );
        schema.define(
            "IfcStructuralLoadPlanarForce",
            IfcStructuralLoadStatic,
            {
                {"PlanarForceX"},
                {"PlanarForceY"},
                {"PlanarForceZ"},
                
            }
        );
        CoreClass* IfcStructuralLoadSingleDisplacement = schema.define(
            "IfcStructuralLoadSingleDisplacement",
            IfcStructuralLoadStatic,
            {
                {"DisplacementX"},
                {"DisplacementY"},
                {"DisplacementZ"},
                {"RotationalDisplacementRX"},
                {"RotationalDisplacementRY"},
                {"RotationalDisplacementRZ"},
                
            }
        );
        schema.define(
            "IfcStructuralLoadSingleDisplacementDistortion",
            IfcStructuralLoadSingleDisplacement,
            {
                {"Distortion"},
                
            }
        );
        CoreClass* IfcStructuralLoadSingleForce = schema.define(
            "IfcStructuralLoadSingleForce",
            IfcStructuralLoadStatic,
            {
                {"ForceX"},
                {"ForceY"},
                {"ForceZ"},
                {"MomentX"},
                {"MomentY"},
                {"MomentZ"},
                
            }
        );
        schema.define(
            "IfcStructuralLoadSingleForceWarping",
            IfcStructuralLoadSingleForce,
            {
                {"WarpingMoment"},
                
            }
        );
        CoreClass* IfcStructuralProfileProperties = schema.define(
            "IfcStructuralProfileProperties",
            IfcGeneralProfileProperties,
            {
                {"TorsionalConstantX"},
                {"MomentOfInertiaYZ"},
                {"MomentOfInertiaY"},
                {"MomentOfInertiaZ"},
                {"WarpingConstant"},
                {"ShearCentreZ"},
                {"ShearCentreY"},
                {"ShearDeformationAreaZ"},
                {"ShearDeformationAreaY"},
                {"MaximumSectionModulusY"},
                {"MinimumSectionModulusY"},
                {"MaximumSectionModulusZ"},
                {"MinimumSectionModulusZ"},
                {"TorsionalSectionModulus"},
                {"CentreOfGravityInX"},
                {"CentreOfGravityInY"},
                
            }
        );
        schema.define(
            "IfcStructuralSteelProfileProperties",
            IfcStructuralProfileProperties,
            {
                {"ShearAreaZ"},
                {"ShearAreaY"},
                {"PlasticShapeFactorY"},
                {"PlasticShapeFactorZ"},
                
            }
        );
        schema.define(
            "IfcSubedge",
            IfcEdge,
            {
                {"ParentEdge"},
                
            }
        );
        CoreClass* IfcSurface = schema.define(
            "IfcSurface",
            IfcGeometricRepresentationItem,
            {
                
            }
        );
        schema.define(
            "IfcSurfaceStyleRendering",
            IfcSurfaceStyleShading,
            {
                {"Transparency"},
                {"DiffuseColour"},
                {"TransmissionColour"},
                {"DiffuseTransmissionColour"},
                {"ReflectionColour"},
                {"SpecularColour"},
                {"SpecularHighlight"},
                {"ReflectanceMethod"},
                
            }
        );
        CoreClass* IfcSweptAreaSolid = schema.define(
            "IfcSweptAreaSolid",
            IfcSolidModel,
            {
                {"SweptArea"},
                {"Position"},
                
            }
        );
        schema.define(
            "IfcSweptDiskSolid",
            IfcSolidModel,
            {
                {"Directrix"},
                {"Radius"},
                {"InnerRadius"},
                {"StartParam"},
                {"EndParam"},
                
            }
        );
        CoreClass* IfcSweptSurface = schema.define(
            "IfcSweptSurface",
            IfcSurface,
            {
                {"SweptCurve"},
                {"Position"},
                
            }
        );
        schema.define(
            "IfcTShapeProfileDef",
            IfcParameterizedProfileDef,
            {
                {"Depth"},
                {"FlangeWidth"},
                {"WebThickness"},
                {"FlangeThickness"},
                {"FilletRadius"},
                {"FlangeEdgeRadius"},
                {"WebEdgeRadius"},
                {"WebSlope"},
                {"FlangeSlope"},
                {"CentreOfGravityInY"},
                
            }
        );
        CoreClass* IfcTerminatorSymbol = schema.define(
            "IfcTerminatorSymbol",
            IfcAnnotationSymbolOccurrence,
            {
                {"AnnotatedCurve"},
                
            }
        );
        CoreClass* IfcTextLiteral = schema.define(
            "IfcTextLiteral",
            IfcGeometricRepresentationItem,
            {
                {"Literal"},
                {"Placement"},
                {"Path"},
                
            }
        );
        schema.define(
            "IfcTextLiteralWithExtent",
            IfcTextLiteral,
            {
                {"Extent"},
                {"BoxAlignment"},
                
            }
        );
        schema.define(
            "IfcTrapeziumProfileDef",
            IfcParameterizedProfileDef,
            {
                {"BottomXDim"},
                {"TopXDim"},
                {"YDim"},
                {"TopXOffset"},
                
            }
        );
        schema.define(
            "IfcTwoDirectionRepeatFactor",
            IfcOneDirectionRepeatFactor,
            {
                {"SecondRepeatFactor"},
                
            }
        );
        CoreClass* IfcTypeObject = schema.define(
            "IfcTypeObject",
            IfcObjectDefinition,
            {
                {"ApplicableOccurrence"},
                {"HasPropertySets"},
                
            }
        );
        CoreClass* IfcTypeProduct = schema.define(
            "IfcTypeProduct",
            IfcTypeObject,
            {
                {"RepresentationMaps"},
                {"Tag"},
                
            }
        );
        schema.define(
            "IfcUShapeProfileDef",
            IfcParameterizedProfileDef,
            {
                {"Depth"},
                {"FlangeWidth"},
                {"WebThickness"},
                {"FlangeThickness"},
                {"FilletRadius"},
                {"EdgeRadius"},
                {"FlangeSlope"},
                {"CentreOfGravityInX"},
                
            }
        );
        schema.define(
            "IfcVector",
            IfcGeometricRepresentationItem,
            {
                {"Orientation"},
                {"Magnitude"},
                
            }
        );
        schema.define(
            "IfcVertexLoop",
            IfcLoop,
            {
                {"LoopVertex"},
                
            }
        );
        schema.define(
            "IfcWindowLiningProperties",
            IfcPropertySetDefinition,
            {
                {"LiningDepth"},
                {"LiningThickness"},
                {"TransomThickness"},
                {"MullionThickness"},
                {"FirstTransomOffset"},
                {"SecondTransomOffset"},
                {"FirstMullionOffset"},
                {"SecondMullionOffset"},
                {"ShapeAspectStyle"},
                
            }
        );
        schema.define(
            "IfcWindowPanelProperties",
            IfcPropertySetDefinition,
            {
                {"OperationType"},
                {"PanelPosition"},
                {"FrameDepth"},
                {"FrameThickness"},
                {"ShapeAspectStyle"},
                
            }
        );
        schema.define(
            "IfcWindowStyle",
            IfcTypeProduct,
            {
                {"ConstructionType"},
                {"OperationType"},
                {"ParameterTakesPrecedence"},
                {"Sizeable"},
                
            }
        );
        schema.define(
            "IfcZShapeProfileDef",
            IfcParameterizedProfileDef,
            {
                {"Depth"},
                {"FlangeWidth"},
                {"WebThickness"},
                {"FlangeThickness"},
                {"FilletRadius"},
                {"EdgeRadius"},
                
            }
        );
        CoreClass* IfcAnnotationCurveOccurrence = schema.define(
            "IfcAnnotationCurveOccurrence",
            IfcAnnotationOccurrence,
            {
                
            }
        );
        schema.define(
            "IfcAnnotationFillArea",
            IfcGeometricRepresentationItem,
            {
                {"OuterBoundary"},
                {"InnerBoundaries"},
                
            }
        );
        schema.define(
            "IfcAnnotationFillAreaOccurrence",
            IfcAnnotationOccurrence,
            {
                {"FillStyleTarget"},
                {"GlobalOrLocal"},
                
            }
        );
        schema.define(
            "IfcAnnotationSurface",
            IfcGeometricRepresentationItem,
            {
                {"Item"},
                {"TextureCoordinates"},
                
            }
        );
        schema.define(
            "IfcAxis1Placement",
            IfcPlacement,
            {
                {"Axis"},
                
            }
        );
        schema.define(
            "IfcAxis2Placement2D",
            IfcPlacement,
            {
                {"RefDirection"},
                
            }
        );
        schema.define(
            "IfcAxis2Placement3D",
            IfcPlacement,
            {
                {"Axis"},
                {"RefDirection"},
                
            }
        );
        CoreClass* IfcBooleanResult = schema.define(
            "IfcBooleanResult",
            IfcGeometricRepresentationItem,
            {
                {"Operator"},
                {"FirstOperand"},
                {"SecondOperand"},
                
            }
        );
        CoreClass* IfcBoundedSurface = schema.define(
            "IfcBoundedSurface",
            IfcSurface,
            {
                
            }
        );
        schema.define(
            "IfcBoundingBox",
            IfcGeometricRepresentationItem,
            {
                {"Corner"},
                {"XDim"},
                {"YDim"},
                {"ZDim"},
                
            }
        );
        schema.define(
            "IfcBoxedHalfSpace",
            IfcHalfSpaceSolid,
            {
                {"Enclosure"},
                
            }
        );
        schema.define(
            "IfcCShapeProfileDef",
            IfcParameterizedProfileDef,
            {
                {"Depth"},
                {"Width"},
                {"WallThickness"},
                {"Girth"},
                {"InternalFilletRadius"},
                {"CentreOfGravityInX"},
                
            }
        );
        schema.define(
            "IfcCartesianPoint",
            IfcPoint,
            {
                {"Coordinates"},
                
            }
        );
        CoreClass* IfcCartesianTransformationOperator = schema.define(
            "IfcCartesianTransformationOperator",
            IfcGeometricRepresentationItem,
            {
                {"Axis1"},
                {"Axis2"},
                {"LocalOrigin"},
                {"Scale"},
                
            }
        );
        CoreClass* IfcCartesianTransformationOperator2D = schema.define(
            "IfcCartesianTransformationOperator2D",
            IfcCartesianTransformationOperator,
            {
                
            }
        );
        schema.define(
            "IfcCartesianTransformationOperator2DnonUniform",
            IfcCartesianTransformationOperator2D,
            {
                {"Scale2"},
                
            }
        );
        CoreClass* IfcCartesianTransformationOperator3D = schema.define(
            "IfcCartesianTransformationOperator3D",
            IfcCartesianTransformationOperator,
            {
                {"Axis3"},
                
            }
        );
        schema.define(
            "IfcCartesianTransformationOperator3DnonUniform",
            IfcCartesianTransformationOperator3D,
            {
                {"Scale2"},
                {"Scale3"},
                
            }
        );
        CoreClass* IfcCircleProfileDef = schema.define(
            "IfcCircleProfileDef",
            IfcParameterizedProfileDef,
            {
                {"Radius"},
                
            }
        );
        schema.define(
            "IfcClosedShell",
            IfcConnectedFaceSet,
            {
                
            }
        );
        schema.define(
            "IfcCompositeCurveSegment",
            IfcGeometricRepresentationItem,
            {
                {"Transition"},
                {"SameSense"},
                {"ParentCurve"},
                
            }
        );
        schema.define(
            "IfcCraneRailAShapeProfileDef",
            IfcParameterizedProfileDef,
            {
                {"OverallHeight"},
                {"BaseWidth2"},
                {"Radius"},
                {"HeadWidth"},
                {"HeadDepth2"},
                {"HeadDepth3"},
                {"WebThickness"},
                {"BaseWidth4"},
                {"BaseDepth1"},
                {"BaseDepth2"},
                {"BaseDepth3"},
                {"CentreOfGravityInY"},
                
            }
        );
        schema.define(
            "IfcCraneRailFShapeProfileDef",
            IfcParameterizedProfileDef,
            {
                {"OverallHeight"},
                {"HeadWidth"},
                {"Radius"},
                {"HeadDepth2"},
                {"HeadDepth3"},
                {"WebThickness"},
                {"BaseDepth1"},
                {"BaseDepth2"},
                {"CentreOfGravityInY"},
                
            }
        );
        CoreClass* IfcCsgPrimitive3D = schema.define(
            "IfcCsgPrimitive3D",
            IfcGeometricRepresentationItem,
            {
                {"Position"},
                
            }
        );
        schema.define(
            "IfcCsgSolid",
            IfcSolidModel,
            {
                {"TreeRootExpression"},
                
            }
        );
        CoreClass* IfcCurve = schema.define(
            "IfcCurve",
            IfcGeometricRepresentationItem,
            {
                
            }
        );
        schema.define(
            "IfcCurveBoundedPlane",
            IfcBoundedSurface,
            {
                {"BasisSurface"},
                {"OuterBoundary"},
                {"InnerBoundaries"},
                
            }
        );
        schema.define(
            "IfcDefinedSymbol",
            IfcGeometricRepresentationItem,
            {
                {"Definition"},
                {"Target"},
                
            }
        );
        schema.define(
            "IfcDimensionCurve",
            IfcAnnotationCurveOccurrence,
            {
                
            }
        );
        schema.define(
            "IfcDimensionCurveTerminator",
            IfcTerminatorSymbol,
            {
                {"Role"},
                
            }
        );
        schema.define(
            "IfcDirection",
            IfcGeometricRepresentationItem,
            {
                {"DirectionRatios"},
                
            }
        );
        schema.define(
            "IfcDoorLiningProperties",
            IfcPropertySetDefinition,
            {
                {"LiningDepth"},
                {"LiningThickness"},
                {"ThresholdDepth"},
                {"ThresholdThickness"},
                {"TransomThickness"},
                {"TransomOffset"},
                {"LiningOffset"},
                {"ThresholdOffset"},
                {"CasingThickness"},
                {"CasingDepth"},
                {"ShapeAspectStyle"},
                
            }
        );
        schema.define(
            "IfcDoorPanelProperties",
            IfcPropertySetDefinition,
            {
                {"PanelDepth"},
                {"PanelOperation"},
                {"PanelWidth"},
                {"PanelPosition"},
                {"ShapeAspectStyle"},
                
            }
        );
        schema.define(
            "IfcDoorStyle",
            IfcTypeProduct,
            {
                {"OperationType"},
                {"ConstructionType"},
                {"ParameterTakesPrecedence"},
                {"Sizeable"},
                
            }
        );
        CoreClass* IfcDraughtingCallout = schema.define(
            "IfcDraughtingCallout",
            IfcGeometricRepresentationItem,
            {
                {"Contents"},
                
            }
        );
        schema.define(
            "IfcDraughtingPreDefinedColour",
            IfcPreDefinedColour,
            {
                
            }
        );
        schema.define(
            "IfcDraughtingPreDefinedCurveFont",
            IfcPreDefinedCurveFont,
            {
                
            }
        );
        schema.define(
            "IfcEdgeLoop",
            IfcLoop,
            {
                {"EdgeList"},
                
            }
        );
        schema.define(
            "IfcElementQuantity",
            IfcPropertySetDefinition,
            {
                {"MethodOfMeasurement"},
                {"Quantities"},
                
            }
        );
        CoreClass* IfcElementType = schema.define(
            "IfcElementType",
            IfcTypeProduct,
            {
                {"ElementType"},
                
            }
        );
        CoreClass* IfcElementarySurface = schema.define(
            "IfcElementarySurface",
            IfcSurface,
            {
                {"Position"},
                
            }
        );
        schema.define(
            "IfcEllipseProfileDef",
            IfcParameterizedProfileDef,
            {
                {"SemiAxis1"},
                {"SemiAxis2"},
                
            }
        );
        CoreClass* IfcEnergyProperties = schema.define(
            "IfcEnergyProperties",
            IfcPropertySetDefinition,
            {
                {"EnergySequence"},
                {"UserDefinedEnergySequence"},
                
            }
        );
        schema.define(
            "IfcExtrudedAreaSolid",
            IfcSweptAreaSolid,
            {
                {"ExtrudedDirection"},
                {"Depth"},
                
            }
        );
        schema.define(
            "IfcFaceBasedSurfaceModel",
            IfcGeometricRepresentationItem,
            {
                {"FbsmFaces"},
                
            }
        );
        schema.define(
            "IfcFillAreaStyleHatching",
            IfcGeometricRepresentationItem,
            {
                {"HatchLineAppearance"},
                {"StartOfNextHatchLine"},
                {"PointOfReferenceHatchLine"},
                {"PatternStart"},
                {"HatchLineAngle"},
                
            }
        );
        schema.define(
            "IfcFillAreaStyleTileSymbolWithStyle",
            IfcGeometricRepresentationItem,
            {
                {"Symbol"},
                
            }
        );
        schema.define(
            "IfcFillAreaStyleTiles",
            IfcGeometricRepresentationItem,
            {
                {"TilingPattern"},
                {"Tiles"},
                {"TilingScale"},
                
            }
        );
        schema.define(
            "IfcFluidFlowProperties",
            IfcPropertySetDefinition,
            {
                {"PropertySource"},
                {"FlowConditionTimeSeries"},
                {"VelocityTimeSeries"},
                {"FlowrateTimeSeries"},
                {"Fluid"},
                {"PressureTimeSeries"},
                {"UserDefinedPropertySource"},
                {"TemperatureSingleValue"},
                {"WetBulbTemperatureSingleValue"},
                {"WetBulbTemperatureTimeSeries"},
                {"TemperatureTimeSeries"},
                {"FlowrateSingleValue"},
                {"FlowConditionSingleValue"},
                {"VelocitySingleValue"},
                {"PressureSingleValue"},
                
            }
        );
        CoreClass* IfcFurnishingElementType = schema.define(
            "IfcFurnishingElementType",
            IfcElementType,
            {
                
            }
        );
        schema.define(
            "IfcFurnitureType",
            IfcFurnishingElementType,
            {
                {"AssemblyPlace"},
                
            }
        );
        schema.define(
            "IfcGeometricCurveSet",
            IfcGeometricSet,
            {
                
            }
        );
        CoreClass* IfcIShapeProfileDef = schema.define(
            "IfcIShapeProfileDef",
            IfcParameterizedProfileDef,
            {
                {"OverallWidth"},
                {"OverallDepth"},
                {"WebThickness"},
                {"FlangeThickness"},
                {"FilletRadius"},
                
            }
        );
        schema.define(
            "IfcLShapeProfileDef",
            IfcParameterizedProfileDef,
            {
                {"Depth"},
                {"Width"},
                {"Thickness"},
                {"FilletRadius"},
                {"EdgeRadius"},
                {"LegSlope"},
                {"CentreOfGravityInX"},
                {"CentreOfGravityInY"},
                
            }
        );
        schema.define(
            "IfcLine",
            IfcCurve,
            {
                {"Pnt"},
                {"Dir"},
                
            }
        );
        CoreClass* IfcManifoldSolidBrep = schema.define(
            "IfcManifoldSolidBrep",
            IfcSolidModel,
            {
                {"Outer"},
                
            }
        );
        CoreClass* IfcObject = schema.define(
            "IfcObject",
            IfcObjectDefinition,
            {
                {"ObjectType"},
                
            }
        );
        schema.define(
            "IfcOffsetCurve2D",
            IfcCurve,
            {
                {"BasisCurve"},
                {"Distance"},
                {"SelfIntersect"},
                
            }
        );
        schema.define(
            "IfcOffsetCurve3D",
            IfcCurve,
            {
                {"BasisCurve"},
                {"Distance"},
                {"SelfIntersect"},
                {"RefDirection"},
                
            }
        );
        schema.define(
            "IfcPermeableCoveringProperties",
            IfcPropertySetDefinition,
            {
                {"OperationType"},
                {"PanelPosition"},
                {"FrameDepth"},
                {"FrameThickness"},
                {"ShapeAspectStyle"},
                
            }
        );
        schema.define(
            "IfcPlanarBox",
            IfcPlanarExtent,
            {
                {"Placement"},
                
            }
        );
        schema.define(
            "IfcPlane",
            IfcElementarySurface,
            {
                
            }
        );
        CoreClass* IfcProcess = schema.define(
            "IfcProcess",
            IfcObject,
            {
                
            }
        );
        CoreClass* IfcProduct = schema.define(
            "IfcProduct",
            IfcObject,
            {
                {"ObjectPlacement"},
                {"Representation"},
                
            }
        );
        schema.define(
            "IfcProject",
            IfcObject,
            {
                {"LongName"},
                {"Phase"},
                {"RepresentationContexts"},
                {"UnitsInContext"},
                
            }
        );
        schema.define(
            "IfcProjectionCurve",
            IfcAnnotationCurveOccurrence,
            {
                
            }
        );
        schema.define(
            "IfcPropertySet",
            IfcPropertySetDefinition,
            {
                {"HasProperties"},
                
            }
        );
        schema.define(
            "IfcProxy",
            IfcProduct,
            {
                {"ProxyType"},
                {"Tag"},
                
            }
        );
        schema.define(
            "IfcRectangleHollowProfileDef",
            IfcRectangleProfileDef,
            {
                {"WallThickness"},
                {"InnerFilletRadius"},
                {"OuterFilletRadius"},
                
            }
        );
        schema.define(
            "IfcRectangularPyramid",
            IfcCsgPrimitive3D,
            {
                {"XLength"},
                {"YLength"},
                {"Height"},
                
            }
        );
        schema.define(
            "IfcRectangularTrimmedSurface",
            IfcBoundedSurface,
            {
                {"BasisSurface"},
                {"U1"},
                {"V1"},
                {"U2"},
                {"V2"},
                {"Usense"},
                {"Vsense"},
                
            }
        );
        CoreClass* IfcRelAssigns = schema.define(
            "IfcRelAssigns",
            IfcRelationship,
            {
                {"RelatedObjects"},
                {"RelatedObjectsType"},
                
            }
        );
        CoreClass* IfcRelAssignsToActor = schema.define(
            "IfcRelAssignsToActor",
            IfcRelAssigns,
            {
                {"RelatingActor"},
                {"ActingRole"},
                
            }
        );
        CoreClass* IfcRelAssignsToControl = schema.define(
            "IfcRelAssignsToControl",
            IfcRelAssigns,
            {
                {"RelatingControl"},
                
            }
        );
        schema.define(
            "IfcRelAssignsToGroup",
            IfcRelAssigns,
            {
                {"RelatingGroup"},
                
            }
        );
        schema.define(
            "IfcRelAssignsToProcess",
            IfcRelAssigns,
            {
                {"RelatingProcess"},
                {"QuantityInProcess"},
                
            }
        );
        schema.define(
            "IfcRelAssignsToProduct",
            IfcRelAssigns,
            {
                {"RelatingProduct"},
                
            }
        );
        schema.define(
            "IfcRelAssignsToProjectOrder",
            IfcRelAssignsToControl,
            {
                
            }
        );
        schema.define(
            "IfcRelAssignsToResource",
            IfcRelAssigns,
            {
                {"RelatingResource"},
                
            }
        );
        CoreClass* IfcRelAssociates = schema.define(
            "IfcRelAssociates",
            IfcRelationship,
            {
                {"RelatedObjects"},
                
            }
        );
        schema.define(
            "IfcRelAssociatesAppliedValue",
            IfcRelAssociates,
            {
                {"RelatingAppliedValue"},
                
            }
        );
        schema.define(
            "IfcRelAssociatesApproval",
            IfcRelAssociates,
            {
                {"RelatingApproval"},
                
            }
        );
        schema.define(
            "IfcRelAssociatesClassification",
            IfcRelAssociates,
            {
                {"RelatingClassification"},
                
            }
        );
        schema.define(
            "IfcRelAssociatesConstraint",
            IfcRelAssociates,
            {
                {"Intent"},
                {"RelatingConstraint"},
                
            }
        );
        schema.define(
            "IfcRelAssociatesDocument",
            IfcRelAssociates,
            {
                {"RelatingDocument"},
                
            }
        );
        schema.define(
            "IfcRelAssociatesLibrary",
            IfcRelAssociates,
            {
                {"RelatingLibrary"},
                
            }
        );
        schema.define(
            "IfcRelAssociatesMaterial",
            IfcRelAssociates,
            {
                {"RelatingMaterial"},
                
            }
        );
        schema.define(
            "IfcRelAssociatesProfileProperties",
            IfcRelAssociates,
            {
                {"RelatingProfileProperties"},
                {"ProfileSectionLocation"},
                {"ProfileOrientation"},
                
            }
        );
        CoreClass* IfcRelConnects = schema.define(
            "IfcRelConnects",
            IfcRelationship,
            {
                
            }
        );
        CoreClass* IfcRelConnectsElements = schema.define(
            "IfcRelConnectsElements",
            IfcRelConnects,
            {
                {"ConnectionGeometry"},
                {"RelatingElement"},
                {"RelatedElement"},
                
            }
        );
        schema.define(
            "IfcRelConnectsPathElements",
            IfcRelConnectsElements,
            {
                {"RelatingPriorities"},
                {"RelatedPriorities"},
                {"RelatedConnectionType"},
                {"RelatingConnectionType"},
                
            }
        );
        schema.define(
            "IfcRelConnectsPortToElement",
            IfcRelConnects,
            {
                {"RelatingPort"},
                {"RelatedElement"},
                
            }
        );
        schema.define(
            "IfcRelConnectsPorts",
            IfcRelConnects,
            {
                {"RelatingPort"},
                {"RelatedPort"},
                {"RealizingElement"},
                
            }
        );
        schema.define(
            "IfcRelConnectsStructuralActivity",
            IfcRelConnects,
            {
                {"RelatingElement"},
                {"RelatedStructuralActivity"},
                
            }
        );
        schema.define(
            "IfcRelConnectsStructuralElement",
            IfcRelConnects,
            {
                {"RelatingElement"},
                {"RelatedStructuralMember"},
                
            }
        );
        CoreClass* IfcRelConnectsStructuralMember = schema.define(
            "IfcRelConnectsStructuralMember",
            IfcRelConnects,
            {
                {"RelatingStructuralMember"},
                {"RelatedStructuralConnection"},
                {"AppliedCondition"},
                {"AdditionalConditions"},
                {"SupportedLength"},
                {"ConditionCoordinateSystem"},
                
            }
        );
        schema.define(
            "IfcRelConnectsWithEccentricity",
            IfcRelConnectsStructuralMember,
            {
                {"ConnectionConstraint"},
                
            }
        );
        schema.define(
            "IfcRelConnectsWithRealizingElements",
            IfcRelConnectsElements,
            {
                {"RealizingElements"},
                {"ConnectionType"},
                
            }
        );
        schema.define(
            "IfcRelContainedInSpatialStructure",
            IfcRelConnects,
            {
                {"RelatedElements"},
                {"RelatingStructure"},
                
            }
        );
        schema.define(
            "IfcRelCoversBldgElements",
            IfcRelConnects,
            {
                {"RelatingBuildingElement"},
                {"RelatedCoverings"},
                
            }
        );
        schema.define(
            "IfcRelCoversSpaces",
            IfcRelConnects,
            {
                {"RelatedSpace"},
                {"RelatedCoverings"},
                
            }
        );
        CoreClass* IfcRelDecomposes = schema.define(
            "IfcRelDecomposes",
            IfcRelationship,
            {
                {"RelatingObject"},
                {"RelatedObjects"},
                
            }
        );
        CoreClass* IfcRelDefines = schema.define(
            "IfcRelDefines",
            IfcRelationship,
            {
                {"RelatedObjects"},
                
            }
        );
        CoreClass* IfcRelDefinesByProperties = schema.define(
            "IfcRelDefinesByProperties",
            IfcRelDefines,
            {
                {"RelatingPropertyDefinition"},
                
            }
        );
        schema.define(
            "IfcRelDefinesByType",
            IfcRelDefines,
            {
                {"RelatingType"},
                
            }
        );
        schema.define(
            "IfcRelFillsElement",
            IfcRelConnects,
            {
                {"RelatingOpeningElement"},
                {"RelatedBuildingElement"},
                
            }
        );
        schema.define(
            "IfcRelFlowControlElements",
            IfcRelConnects,
            {
                {"RelatedControlElements"},
                {"RelatingFlowElement"},
                
            }
        );
        schema.define(
            "IfcRelInteractionRequirements",
            IfcRelConnects,
            {
                {"DailyInteraction"},
                {"ImportanceRating"},
                {"LocationOfInteraction"},
                {"RelatedSpaceProgram"},
                {"RelatingSpaceProgram"},
                
            }
        );
        schema.define(
            "IfcRelNests",
            IfcRelDecomposes,
            {
                
            }
        );
        schema.define(
            "IfcRelOccupiesSpaces",
            IfcRelAssignsToActor,
            {
                
            }
        );
        schema.define(
            "IfcRelOverridesProperties",
            IfcRelDefinesByProperties,
            {
                {"OverridingProperties"},
                
            }
        );
        schema.define(
            "IfcRelProjectsElement",
            IfcRelConnects,
            {
                {"RelatingElement"},
                {"RelatedFeatureElement"},
                
            }
        );
        schema.define(
            "IfcRelReferencedInSpatialStructure",
            IfcRelConnects,
            {
                {"RelatedElements"},
                {"RelatingStructure"},
                
            }
        );
        schema.define(
            "IfcRelSchedulesCostItems",
            IfcRelAssignsToControl,
            {
                
            }
        );
        schema.define(
            "IfcRelSequence",
            IfcRelConnects,
            {
                {"RelatingProcess"},
                {"RelatedProcess"},
                {"TimeLag"},
                {"SequenceType"},
                
            }
        );
        schema.define(
            "IfcRelServicesBuildings",
            IfcRelConnects,
            {
                {"RelatingSystem"},
                {"RelatedBuildings"},
                
            }
        );
        schema.define(
            "IfcRelSpaceBoundary",
            IfcRelConnects,
            {
                {"RelatingSpace"},
                {"RelatedBuildingElement"},
                {"ConnectionGeometry"},
                {"PhysicalOrVirtualBoundary"},
                {"InternalOrExternalBoundary"},
                
            }
        );
        schema.define(
            "IfcRelVoidsElement",
            IfcRelConnects,
            {
                {"RelatingBuildingElement"},
                {"RelatedOpeningElement"},
                
            }
        );
        CoreClass* IfcResource = schema.define(
            "IfcResource",
            IfcObject,
            {
                
            }
        );
        schema.define(
            "IfcRevolvedAreaSolid",
            IfcSweptAreaSolid,
            {
                {"Axis"},
                {"Angle"},
                
            }
        );
        schema.define(
            "IfcRightCircularCone",
            IfcCsgPrimitive3D,
            {
                {"Height"},
                {"BottomRadius"},
                
            }
        );
        schema.define(
            "IfcRightCircularCylinder",
            IfcCsgPrimitive3D,
            {
                {"Height"},
                {"Radius"},
                
            }
        );
        CoreClass* IfcSpatialStructureElement = schema.define(
            "IfcSpatialStructureElement",
            IfcProduct,
            {
                {"LongName"},
                {"CompositionType"},
                
            }
        );
        CoreClass* IfcSpatialStructureElementType = schema.define(
            "IfcSpatialStructureElementType",
            IfcElementType,
            {
                
            }
        );
        schema.define(
            "IfcSphere",
            IfcCsgPrimitive3D,
            {
                {"Radius"},
                
            }
        );
        CoreClass* IfcStructuralActivity = schema.define(
            "IfcStructuralActivity",
            IfcProduct,
            {
                {"AppliedLoad"},
                {"GlobalOrLocal"},
                
            }
        );
        CoreClass* IfcStructuralItem = schema.define(
            "IfcStructuralItem",
            IfcProduct,
            {
                
            }
        );
        CoreClass* IfcStructuralMember = schema.define(
            "IfcStructuralMember",
            IfcStructuralItem,
            {
                
            }
        );
        CoreClass* IfcStructuralReaction = schema.define(
            "IfcStructuralReaction",
            IfcStructuralActivity,
            {
                
            }
        );
        CoreClass* IfcStructuralSurfaceMember = schema.define(
            "IfcStructuralSurfaceMember",
            IfcStructuralMember,
            {
                {"PredefinedType"},
                {"Thickness"},
                
            }
        );
        schema.define(
            "IfcStructuralSurfaceMemberVarying",
            IfcStructuralSurfaceMember,
            {
                {"SubsequentThickness"},
                {"VaryingThicknessLocation"},
                
            }
        );
        schema.define(
            "IfcStructuredDimensionCallout",
            IfcDraughtingCallout,
            {
                
            }
        );
        schema.define(
            "IfcSurfaceCurveSweptAreaSolid",
            IfcSweptAreaSolid,
            {
                {"Directrix"},
                {"StartParam"},
                {"EndParam"},
                {"ReferenceSurface"},
                
            }
        );
        schema.define(
            "IfcSurfaceOfLinearExtrusion",
            IfcSweptSurface,
            {
                {"ExtrudedDirection"},
                {"Depth"},
                
            }
        );
        schema.define(
            "IfcSurfaceOfRevolution",
            IfcSweptSurface,
            {
                {"AxisPosition"},
                
            }
        );
        schema.define(
            "IfcSystemFurnitureElementType",
            IfcFurnishingElementType,
            {
                
            }
        );
        CoreClass* IfcTask = schema.define(
            "IfcTask",
            IfcProcess,
            {
                {"TaskId"},
                {"Status"},
                {"WorkMethod"},
                {"IsMilestone"},
                {"Priority"},
                
            }
        );
        schema.define(
            "IfcTransportElementType",
            IfcElementType,
            {
                {"PredefinedType"},
                
            }
        );
        CoreClass* IfcActor = schema.define(
            "IfcActor",
            IfcObject,
            {
                {"TheActor"},
                
            }
        );
        schema.define(
            "IfcAnnotation",
            IfcProduct,
            {
                
            }
        );
        schema.define(
            "IfcAsymmetricIShapeProfileDef",
            IfcIShapeProfileDef,
            {
                {"TopFlangeWidth"},
                {"TopFlangeThickness"},
                {"TopFlangeFilletRadius"},
                {"CentreOfGravityInY"},
                
            }
        );
        schema.define(
            "IfcBlock",
            IfcCsgPrimitive3D,
            {
                {"XLength"},
                {"YLength"},
                {"ZLength"},
                
            }
        );
        schema.define(
            "IfcBooleanClippingResult",
            IfcBooleanResult,
            {
                
            }
        );
        CoreClass* IfcBoundedCurve = schema.define(
            "IfcBoundedCurve",
            IfcCurve,
            {
                
            }
        );
        schema.define(
            "IfcBuilding",
            IfcSpatialStructureElement,
            {
                {"ElevationOfRefHeight"},
                {"ElevationOfTerrain"},
                {"BuildingAddress"},
                
            }
        );
        CoreClass* IfcBuildingElementType = schema.define(
            "IfcBuildingElementType",
            IfcElementType,
            {
                
            }
        );
        schema.define(
            "IfcBuildingStorey",
            IfcSpatialStructureElement,
            {
                {"Elevation"},
                
            }
        );
        schema.define(
            "IfcCircleHollowProfileDef",
            IfcCircleProfileDef,
            {
                {"WallThickness"},
                
            }
        );
        schema.define(
            "IfcColumnType",
            IfcBuildingElementType,
            {
                {"PredefinedType"},
                
            }
        );
        CoreClass* IfcCompositeCurve = schema.define(
            "IfcCompositeCurve",
            IfcBoundedCurve,
            {
                {"Segments"},
                {"SelfIntersect"},
                
            }
        );
        CoreClass* IfcConic = schema.define(
            "IfcConic",
            IfcCurve,
            {
                {"Position"},
                
            }
        );
        CoreClass* IfcConstructionResource = schema.define(
            "IfcConstructionResource",
            IfcResource,
            {
                {"ResourceIdentifier"},
                {"ResourceGroup"},
                {"ResourceConsumption"},
                {"BaseQuantity"},
                
            }
        );
        CoreClass* IfcControl = schema.define(
            "IfcControl",
            IfcObject,
            {
                
            }
        );
        schema.define(
            "IfcCostItem",
            IfcControl,
            {
                
            }
        );
        schema.define(
            "IfcCostSchedule",
            IfcControl,
            {
                {"SubmittedBy"},
                {"PreparedBy"},
                {"SubmittedOn"},
                {"Status"},
                {"TargetUsers"},
                {"UpdateDate"},
                {"ID"},
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcCoveringType",
            IfcBuildingElementType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcCrewResource",
            IfcConstructionResource,
            {
                
            }
        );
        schema.define(
            "IfcCurtainWallType",
            IfcBuildingElementType,
            {
                {"PredefinedType"},
                
            }
        );
        CoreClass* IfcDimensionCurveDirectedCallout = schema.define(
            "IfcDimensionCurveDirectedCallout",
            IfcDraughtingCallout,
            {
                
            }
        );
        CoreClass* IfcDistributionElementType = schema.define(
            "IfcDistributionElementType",
            IfcElementType,
            {
                
            }
        );
        CoreClass* IfcDistributionFlowElementType = schema.define(
            "IfcDistributionFlowElementType",
            IfcDistributionElementType,
            {
                
            }
        );
        schema.define(
            "IfcElectricalBaseProperties",
            IfcEnergyProperties,
            {
                {"ElectricCurrentType"},
                {"InputVoltage"},
                {"InputFrequency"},
                {"FullLoadCurrent"},
                {"MinimumCircuitCurrent"},
                {"MaximumPowerInput"},
                {"RatedPowerInput"},
                {"InputPhase"},
                
            }
        );
        CoreClass* IfcElement = schema.define(
            "IfcElement",
            IfcProduct,
            {
                {"Tag"},
                
            }
        );
        schema.define(
            "IfcElementAssembly",
            IfcElement,
            {
                {"AssemblyPlace"},
                {"PredefinedType"},
                
            }
        );
        CoreClass* IfcElementComponent = schema.define(
            "IfcElementComponent",
            IfcElement,
            {
                
            }
        );
        CoreClass* IfcElementComponentType = schema.define(
            "IfcElementComponentType",
            IfcElementType,
            {
                
            }
        );
        schema.define(
            "IfcEllipse",
            IfcConic,
            {
                {"SemiAxis1"},
                {"SemiAxis2"},
                
            }
        );
        CoreClass* IfcEnergyConversionDeviceType = schema.define(
            "IfcEnergyConversionDeviceType",
            IfcDistributionFlowElementType,
            {
                
            }
        );
        schema.define(
            "IfcEquipmentElement",
            IfcElement,
            {
                
            }
        );
        schema.define(
            "IfcEquipmentStandard",
            IfcControl,
            {
                
            }
        );
        schema.define(
            "IfcEvaporativeCoolerType",
            IfcEnergyConversionDeviceType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcEvaporatorType",
            IfcEnergyConversionDeviceType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcFacetedBrep",
            IfcManifoldSolidBrep,
            {
                
            }
        );
        schema.define(
            "IfcFacetedBrepWithVoids",
            IfcManifoldSolidBrep,
            {
                {"Voids"},
                
            }
        );
        CoreClass* IfcFastener = schema.define(
            "IfcFastener",
            IfcElementComponent,
            {
                
            }
        );
        CoreClass* IfcFastenerType = schema.define(
            "IfcFastenerType",
            IfcElementComponentType,
            {
                
            }
        );
        CoreClass* IfcFeatureElement = schema.define(
            "IfcFeatureElement",
            IfcElement,
            {
                
            }
        );
        CoreClass* IfcFeatureElementAddition = schema.define(
            "IfcFeatureElementAddition",
            IfcFeatureElement,
            {
                
            }
        );
        CoreClass* IfcFeatureElementSubtraction = schema.define(
            "IfcFeatureElementSubtraction",
            IfcFeatureElement,
            {
                
            }
        );
        CoreClass* IfcFlowControllerType = schema.define(
            "IfcFlowControllerType",
            IfcDistributionFlowElementType,
            {
                
            }
        );
        CoreClass* IfcFlowFittingType = schema.define(
            "IfcFlowFittingType",
            IfcDistributionFlowElementType,
            {
                
            }
        );
        schema.define(
            "IfcFlowMeterType",
            IfcFlowControllerType,
            {
                {"PredefinedType"},
                
            }
        );
        CoreClass* IfcFlowMovingDeviceType = schema.define(
            "IfcFlowMovingDeviceType",
            IfcDistributionFlowElementType,
            {
                
            }
        );
        CoreClass* IfcFlowSegmentType = schema.define(
            "IfcFlowSegmentType",
            IfcDistributionFlowElementType,
            {
                
            }
        );
        CoreClass* IfcFlowStorageDeviceType = schema.define(
            "IfcFlowStorageDeviceType",
            IfcDistributionFlowElementType,
            {
                
            }
        );
        CoreClass* IfcFlowTerminalType = schema.define(
            "IfcFlowTerminalType",
            IfcDistributionFlowElementType,
            {
                
            }
        );
        CoreClass* IfcFlowTreatmentDeviceType = schema.define(
            "IfcFlowTreatmentDeviceType",
            IfcDistributionFlowElementType,
            {
                
            }
        );
        schema.define(
            "IfcFurnishingElement",
            IfcElement,
            {
                
            }
        );
        schema.define(
            "IfcFurnitureStandard",
            IfcControl,
            {
                
            }
        );
        schema.define(
            "IfcGasTerminalType",
            IfcFlowTerminalType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcGrid",
            IfcProduct,
            {
                {"UAxes"},
                {"VAxes"},
                {"WAxes"},
                
            }
        );
        CoreClass* IfcGroup = schema.define(
            "IfcGroup",
            IfcObject,
            {
                
            }
        );
        schema.define(
            "IfcHeatExchangerType",
            IfcEnergyConversionDeviceType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcHumidifierType",
            IfcEnergyConversionDeviceType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcInventory",
            IfcGroup,
            {
                {"InventoryType"},
                {"Jurisdiction"},
                {"ResponsiblePersons"},
                {"LastUpdateDate"},
                {"CurrentValue"},
                {"OriginalValue"},
                
            }
        );
        schema.define(
            "IfcJunctionBoxType",
            IfcFlowFittingType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcLaborResource",
            IfcConstructionResource,
            {
                {"SkillSet"},
                
            }
        );
        schema.define(
            "IfcLampType",
            IfcFlowTerminalType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcLightFixtureType",
            IfcFlowTerminalType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcLinearDimension",
            IfcDimensionCurveDirectedCallout,
            {
                
            }
        );
        schema.define(
            "IfcMechanicalFastener",
            IfcFastener,
            {
                {"NominalDiameter"},
                {"NominalLength"},
                
            }
        );
        schema.define(
            "IfcMechanicalFastenerType",
            IfcFastenerType,
            {
                
            }
        );
        schema.define(
            "IfcMemberType",
            IfcBuildingElementType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcMotorConnectionType",
            IfcEnergyConversionDeviceType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcMove",
            IfcTask,
            {
                {"MoveFrom"},
                {"MoveTo"},
                {"PunchList"},
                
            }
        );
        schema.define(
            "IfcOccupant",
            IfcActor,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcOpeningElement",
            IfcFeatureElementSubtraction,
            {
                
            }
        );
        schema.define(
            "IfcOrderAction",
            IfcTask,
            {
                {"ActionID"},
                
            }
        );
        schema.define(
            "IfcOutletType",
            IfcFlowTerminalType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcPerformanceHistory",
            IfcControl,
            {
                {"LifeCyclePhase"},
                
            }
        );
        schema.define(
            "IfcPermit",
            IfcControl,
            {
                {"PermitID"},
                
            }
        );
        schema.define(
            "IfcPipeFittingType",
            IfcFlowFittingType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcPipeSegmentType",
            IfcFlowSegmentType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcPlateType",
            IfcBuildingElementType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcPolyline",
            IfcBoundedCurve,
            {
                {"Points"},
                
            }
        );
        CoreClass* IfcPort = schema.define(
            "IfcPort",
            IfcProduct,
            {
                
            }
        );
        schema.define(
            "IfcProcedure",
            IfcProcess,
            {
                {"ProcedureID"},
                {"ProcedureType"},
                {"UserDefinedProcedureType"},
                
            }
        );
        schema.define(
            "IfcProjectOrder",
            IfcControl,
            {
                {"ID"},
                {"PredefinedType"},
                {"Status"},
                
            }
        );
        schema.define(
            "IfcProjectOrderRecord",
            IfcControl,
            {
                {"Records"},
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcProjectionElement",
            IfcFeatureElementAddition,
            {
                
            }
        );
        schema.define(
            "IfcProtectiveDeviceType",
            IfcFlowControllerType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcPumpType",
            IfcFlowMovingDeviceType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcRadiusDimension",
            IfcDimensionCurveDirectedCallout,
            {
                
            }
        );
        schema.define(
            "IfcRailingType",
            IfcBuildingElementType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcRampFlightType",
            IfcBuildingElementType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcRelAggregates",
            IfcRelDecomposes,
            {
                
            }
        );
        schema.define(
            "IfcRelAssignsTasks",
            IfcRelAssignsToControl,
            {
                {"TimeForTask"},
                
            }
        );
        schema.define(
            "IfcSanitaryTerminalType",
            IfcFlowTerminalType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcScheduleTimeControl",
            IfcControl,
            {
                {"ActualStart"},
                {"EarlyStart"},
                {"LateStart"},
                {"ScheduleStart"},
                {"ActualFinish"},
                {"EarlyFinish"},
                {"LateFinish"},
                {"ScheduleFinish"},
                {"ScheduleDuration"},
                {"ActualDuration"},
                {"RemainingTime"},
                {"FreeFloat"},
                {"TotalFloat"},
                {"IsCritical"},
                {"StatusTime"},
                {"StartFloat"},
                {"FinishFloat"},
                {"Completion"},
                
            }
        );
        schema.define(
            "IfcServiceLife",
            IfcControl,
            {
                {"ServiceLifeType"},
                {"ServiceLifeDuration"},
                
            }
        );
        schema.define(
            "IfcSite",
            IfcSpatialStructureElement,
            {
                {"RefLatitude"},
                {"RefLongitude"},
                {"RefElevation"},
                {"LandTitleNumber"},
                {"SiteAddress"},
                
            }
        );
        schema.define(
            "IfcSlabType",
            IfcBuildingElementType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcSpace",
            IfcSpatialStructureElement,
            {
                {"InteriorOrExteriorSpace"},
                {"ElevationWithFlooring"},
                
            }
        );
        schema.define(
            "IfcSpaceHeaterType",
            IfcEnergyConversionDeviceType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcSpaceProgram",
            IfcControl,
            {
                {"SpaceProgramIdentifier"},
                {"MaxRequiredArea"},
                {"MinRequiredArea"},
                {"RequestedLocation"},
                {"StandardRequiredArea"},
                
            }
        );
        schema.define(
            "IfcSpaceType",
            IfcSpatialStructureElementType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcStackTerminalType",
            IfcFlowTerminalType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcStairFlightType",
            IfcBuildingElementType,
            {
                {"PredefinedType"},
                
            }
        );
        CoreClass* IfcStructuralAction = schema.define(
            "IfcStructuralAction",
            IfcStructuralActivity,
            {
                {"DestabilizingLoad"},
                {"CausedBy"},
                
            }
        );
        CoreClass* IfcStructuralConnection = schema.define(
            "IfcStructuralConnection",
            IfcStructuralItem,
            {
                {"AppliedCondition"},
                
            }
        );
        schema.define(
            "IfcStructuralCurveConnection",
            IfcStructuralConnection,
            {
                
            }
        );
        CoreClass* IfcStructuralCurveMember = schema.define(
            "IfcStructuralCurveMember",
            IfcStructuralMember,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcStructuralCurveMemberVarying",
            IfcStructuralCurveMember,
            {
                
            }
        );
        CoreClass* IfcStructuralLinearAction = schema.define(
            "IfcStructuralLinearAction",
            IfcStructuralAction,
            {
                {"ProjectedOrTrue"},
                
            }
        );
        schema.define(
            "IfcStructuralLinearActionVarying",
            IfcStructuralLinearAction,
            {
                {"VaryingAppliedLoadLocation"},
                {"SubsequentAppliedLoads"},
                
            }
        );
        schema.define(
            "IfcStructuralLoadGroup",
            IfcGroup,
            {
                {"PredefinedType"},
                {"ActionType"},
                {"ActionSource"},
                {"Coefficient"},
                {"Purpose"},
                
            }
        );
        CoreClass* IfcStructuralPlanarAction = schema.define(
            "IfcStructuralPlanarAction",
            IfcStructuralAction,
            {
                {"ProjectedOrTrue"},
                
            }
        );
        schema.define(
            "IfcStructuralPlanarActionVarying",
            IfcStructuralPlanarAction,
            {
                {"VaryingAppliedLoadLocation"},
                {"SubsequentAppliedLoads"},
                
            }
        );
        schema.define(
            "IfcStructuralPointAction",
            IfcStructuralAction,
            {
                
            }
        );
        schema.define(
            "IfcStructuralPointConnection",
            IfcStructuralConnection,
            {
                
            }
        );
        schema.define(
            "IfcStructuralPointReaction",
            IfcStructuralReaction,
            {
                
            }
        );
        schema.define(
            "IfcStructuralResultGroup",
            IfcGroup,
            {
                {"TheoryType"},
                {"ResultForLoadGroup"},
                {"IsLinear"},
                
            }
        );
        schema.define(
            "IfcStructuralSurfaceConnection",
            IfcStructuralConnection,
            {
                
            }
        );
        schema.define(
            "IfcSubContractResource",
            IfcConstructionResource,
            {
                {"SubContractor"},
                {"JobDescription"},
                
            }
        );
        schema.define(
            "IfcSwitchingDeviceType",
            IfcFlowControllerType,
            {
                {"PredefinedType"},
                
            }
        );
        CoreClass* IfcSystem = schema.define(
            "IfcSystem",
            IfcGroup,
            {
                
            }
        );
        schema.define(
            "IfcTankType",
            IfcFlowStorageDeviceType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcTimeSeriesSchedule",
            IfcControl,
            {
                {"ApplicableDates"},
                {"TimeSeriesScheduleType"},
                {"TimeSeries"},
                
            }
        );
        schema.define(
            "IfcTransformerType",
            IfcEnergyConversionDeviceType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcTransportElement",
            IfcElement,
            {
                {"OperationType"},
                {"CapacityByWeight"},
                {"CapacityByNumber"},
                
            }
        );
        schema.define(
            "IfcTrimmedCurve",
            IfcBoundedCurve,
            {
                {"BasisCurve"},
                {"Trim1"},
                {"Trim2"},
                {"SenseAgreement"},
                {"MasterRepresentation"},
                
            }
        );
        schema.define(
            "IfcTubeBundleType",
            IfcEnergyConversionDeviceType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcUnitaryEquipmentType",
            IfcEnergyConversionDeviceType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcValveType",
            IfcFlowControllerType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcVirtualElement",
            IfcElement,
            {
                
            }
        );
        schema.define(
            "IfcWallType",
            IfcBuildingElementType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcWasteTerminalType",
            IfcFlowTerminalType,
            {
                {"PredefinedType"},
                
            }
        );
        CoreClass* IfcWorkControl = schema.define(
            "IfcWorkControl",
            IfcControl,
            {
                {"Identifier"},
                {"CreationDate"},
                {"Creators"},
                {"Purpose"},
                {"Duration"},
                {"TotalFloat"},
                {"StartTime"},
                {"FinishTime"},
                {"WorkControlType"},
                {"UserDefinedControlType"},
                
            }
        );
        schema.define(
            "IfcWorkPlan",
            IfcWorkControl,
            {
                
            }
        );
        schema.define(
            "IfcWorkSchedule",
            IfcWorkControl,
            {
                
            }
        );
        schema.define(
            "IfcZone",
            IfcGroup,
            {
                
            }
        );
        schema.define(
            "Ifc2DCompositeCurve",
            IfcCompositeCurve,
            {
                
            }
        );
        schema.define(
            "IfcActionRequest",
            IfcControl,
            {
                {"RequestID"},
                
            }
        );
        schema.define(
            "IfcAirTerminalBoxType",
            IfcFlowControllerType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcAirTerminalType",
            IfcFlowTerminalType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcAirToAirHeatRecoveryType",
            IfcEnergyConversionDeviceType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcAngularDimension",
            IfcDimensionCurveDirectedCallout,
            {
                
            }
        );
        schema.define(
            "IfcAsset",
            IfcGroup,
            {
                {"AssetID"},
                {"OriginalValue"},
                {"CurrentValue"},
                {"TotalReplacementCost"},
                {"Owner"},
                {"User"},
                {"ResponsiblePerson"},
                {"IncorporationDate"},
                {"DepreciatedValue"},
                
            }
        );
        CoreClass* IfcBSplineCurve = schema.define(
            "IfcBSplineCurve",
            IfcBoundedCurve,
            {
                {"Degree"},
                {"ControlPointsList"},
                {"CurveForm"},
                {"ClosedCurve"},
                {"SelfIntersect"},
                
            }
        );
        schema.define(
            "IfcBeamType",
            IfcBuildingElementType,
            {
                {"PredefinedType"},
                
            }
        );
        CoreClass* IfcBezierCurve = schema.define(
            "IfcBezierCurve",
            IfcBSplineCurve,
            {
                
            }
        );
        schema.define(
            "IfcBoilerType",
            IfcEnergyConversionDeviceType,
            {
                {"PredefinedType"},
                
            }
        );
        CoreClass* IfcBuildingElement = schema.define(
            "IfcBuildingElement",
            IfcElement,
            {
                
            }
        );
        CoreClass* IfcBuildingElementComponent = schema.define(
            "IfcBuildingElementComponent",
            IfcBuildingElement,
            {
                
            }
        );
        schema.define(
            "IfcBuildingElementPart",
            IfcBuildingElementComponent,
            {
                
            }
        );
        schema.define(
            "IfcBuildingElementProxy",
            IfcBuildingElement,
            {
                {"CompositionType"},
                
            }
        );
        schema.define(
            "IfcBuildingElementProxyType",
            IfcBuildingElementType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcCableCarrierFittingType",
            IfcFlowFittingType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcCableCarrierSegmentType",
            IfcFlowSegmentType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcCableSegmentType",
            IfcFlowSegmentType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcChillerType",
            IfcEnergyConversionDeviceType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcCircle",
            IfcConic,
            {
                {"Radius"},
                
            }
        );
        schema.define(
            "IfcCoilType",
            IfcEnergyConversionDeviceType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcColumn",
            IfcBuildingElement,
            {
                
            }
        );
        schema.define(
            "IfcCompressorType",
            IfcFlowMovingDeviceType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcCondenserType",
            IfcEnergyConversionDeviceType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcCondition",
            IfcGroup,
            {
                
            }
        );
        schema.define(
            "IfcConditionCriterion",
            IfcControl,
            {
                {"Criterion"},
                {"CriterionDateTime"},
                
            }
        );
        schema.define(
            "IfcConstructionEquipmentResource",
            IfcConstructionResource,
            {
                
            }
        );
        schema.define(
            "IfcConstructionMaterialResource",
            IfcConstructionResource,
            {
                {"Suppliers"},
                {"UsageRatio"},
                
            }
        );
        schema.define(
            "IfcConstructionProductResource",
            IfcConstructionResource,
            {
                
            }
        );
        schema.define(
            "IfcCooledBeamType",
            IfcEnergyConversionDeviceType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcCoolingTowerType",
            IfcEnergyConversionDeviceType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcCovering",
            IfcBuildingElement,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcCurtainWall",
            IfcBuildingElement,
            {
                
            }
        );
        schema.define(
            "IfcDamperType",
            IfcFlowControllerType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcDiameterDimension",
            IfcDimensionCurveDirectedCallout,
            {
                
            }
        );
        schema.define(
            "IfcDiscreteAccessory",
            IfcElementComponent,
            {
                
            }
        );
        CoreClass* IfcDiscreteAccessoryType = schema.define(
            "IfcDiscreteAccessoryType",
            IfcElementComponentType,
            {
                
            }
        );
        schema.define(
            "IfcDistributionChamberElementType",
            IfcDistributionFlowElementType,
            {
                {"PredefinedType"},
                
            }
        );
        CoreClass* IfcDistributionControlElementType = schema.define(
            "IfcDistributionControlElementType",
            IfcDistributionElementType,
            {
                
            }
        );
        CoreClass* IfcDistributionElement = schema.define(
            "IfcDistributionElement",
            IfcElement,
            {
                
            }
        );
        CoreClass* IfcDistributionFlowElement = schema.define(
            "IfcDistributionFlowElement",
            IfcDistributionElement,
            {
                
            }
        );
        schema.define(
            "IfcDistributionPort",
            IfcPort,
            {
                {"FlowDirection"},
                
            }
        );
        schema.define(
            "IfcDoor",
            IfcBuildingElement,
            {
                {"OverallHeight"},
                {"OverallWidth"},
                
            }
        );
        schema.define(
            "IfcDuctFittingType",
            IfcFlowFittingType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcDuctSegmentType",
            IfcFlowSegmentType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcDuctSilencerType",
            IfcFlowTreatmentDeviceType,
            {
                {"PredefinedType"},
                
            }
        );
        CoreClass* IfcEdgeFeature = schema.define(
            "IfcEdgeFeature",
            IfcFeatureElementSubtraction,
            {
                {"FeatureLength"},
                
            }
        );
        schema.define(
            "IfcElectricApplianceType",
            IfcFlowTerminalType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcElectricFlowStorageDeviceType",
            IfcFlowStorageDeviceType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcElectricGeneratorType",
            IfcEnergyConversionDeviceType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcElectricHeaterType",
            IfcFlowTerminalType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcElectricMotorType",
            IfcEnergyConversionDeviceType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcElectricTimeControlType",
            IfcFlowControllerType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcElectricalCircuit",
            IfcSystem,
            {
                
            }
        );
        schema.define(
            "IfcElectricalElement",
            IfcElement,
            {
                
            }
        );
        schema.define(
            "IfcEnergyConversionDevice",
            IfcDistributionFlowElement,
            {
                
            }
        );
        schema.define(
            "IfcFanType",
            IfcFlowMovingDeviceType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcFilterType",
            IfcFlowTreatmentDeviceType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcFireSuppressionTerminalType",
            IfcFlowTerminalType,
            {
                {"PredefinedType"},
                
            }
        );
        CoreClass* IfcFlowController = schema.define(
            "IfcFlowController",
            IfcDistributionFlowElement,
            {
                
            }
        );
        schema.define(
            "IfcFlowFitting",
            IfcDistributionFlowElement,
            {
                
            }
        );
        schema.define(
            "IfcFlowInstrumentType",
            IfcDistributionControlElementType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcFlowMovingDevice",
            IfcDistributionFlowElement,
            {
                
            }
        );
        schema.define(
            "IfcFlowSegment",
            IfcDistributionFlowElement,
            {
                
            }
        );
        schema.define(
            "IfcFlowStorageDevice",
            IfcDistributionFlowElement,
            {
                
            }
        );
        schema.define(
            "IfcFlowTerminal",
            IfcDistributionFlowElement,
            {
                
            }
        );
        schema.define(
            "IfcFlowTreatmentDevice",
            IfcDistributionFlowElement,
            {
                
            }
        );
        schema.define(
            "IfcFooting",
            IfcBuildingElement,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcMember",
            IfcBuildingElement,
            {
                
            }
        );
        schema.define(
            "IfcPile",
            IfcBuildingElement,
            {
                {"PredefinedType"},
                {"ConstructionType"},
                
            }
        );
        schema.define(
            "IfcPlate",
            IfcBuildingElement,
            {
                
            }
        );
        schema.define(
            "IfcRailing",
            IfcBuildingElement,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcRamp",
            IfcBuildingElement,
            {
                {"ShapeType"},
                
            }
        );
        schema.define(
            "IfcRampFlight",
            IfcBuildingElement,
            {
                
            }
        );
        schema.define(
            "IfcRationalBezierCurve",
            IfcBezierCurve,
            {
                {"WeightsData"},
                
            }
        );
        CoreClass* IfcReinforcingElement = schema.define(
            "IfcReinforcingElement",
            IfcBuildingElementComponent,
            {
                {"SteelGrade"},
                
            }
        );
        schema.define(
            "IfcReinforcingMesh",
            IfcReinforcingElement,
            {
                {"MeshLength"},
                {"MeshWidth"},
                {"LongitudinalBarNominalDiameter"},
                {"TransverseBarNominalDiameter"},
                {"LongitudinalBarCrossSectionArea"},
                {"TransverseBarCrossSectionArea"},
                {"LongitudinalBarSpacing"},
                {"TransverseBarSpacing"},
                
            }
        );
        schema.define(
            "IfcRoof",
            IfcBuildingElement,
            {
                {"ShapeType"},
                
            }
        );
        schema.define(
            "IfcRoundedEdgeFeature",
            IfcEdgeFeature,
            {
                {"Radius"},
                
            }
        );
        schema.define(
            "IfcSensorType",
            IfcDistributionControlElementType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcSlab",
            IfcBuildingElement,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcStair",
            IfcBuildingElement,
            {
                {"ShapeType"},
                
            }
        );
        schema.define(
            "IfcStairFlight",
            IfcBuildingElement,
            {
                {"NumberOfRiser"},
                {"NumberOfTreads"},
                {"RiserHeight"},
                {"TreadLength"},
                
            }
        );
        schema.define(
            "IfcStructuralAnalysisModel",
            IfcSystem,
            {
                {"PredefinedType"},
                {"OrientationOf2DPlane"},
                {"LoadedBy"},
                {"HasResults"},
                
            }
        );
        schema.define(
            "IfcTendon",
            IfcReinforcingElement,
            {
                {"PredefinedType"},
                {"NominalDiameter"},
                {"CrossSectionArea"},
                {"TensionForce"},
                {"PreStress"},
                {"FrictionCoefficient"},
                {"AnchorageSlip"},
                {"MinCurvatureRadius"},
                
            }
        );
        schema.define(
            "IfcTendonAnchor",
            IfcReinforcingElement,
            {
                
            }
        );
        schema.define(
            "IfcVibrationIsolatorType",
            IfcDiscreteAccessoryType,
            {
                {"PredefinedType"},
                
            }
        );
        CoreClass* IfcWall = schema.define(
            "IfcWall",
            IfcBuildingElement,
            {
                
            }
        );
        schema.define(
            "IfcWallStandardCase",
            IfcWall,
            {
                
            }
        );
        schema.define(
            "IfcWindow",
            IfcBuildingElement,
            {
                {"OverallHeight"},
                {"OverallWidth"},
                
            }
        );
        schema.define(
            "IfcActuatorType",
            IfcDistributionControlElementType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcAlarmType",
            IfcDistributionControlElementType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcBeam",
            IfcBuildingElement,
            {
                
            }
        );
        schema.define(
            "IfcChamferEdgeFeature",
            IfcEdgeFeature,
            {
                {"Width"},
                {"Height"},
                
            }
        );
        schema.define(
            "IfcControllerType",
            IfcDistributionControlElementType,
            {
                {"PredefinedType"},
                
            }
        );
        schema.define(
            "IfcDistributionChamberElement",
            IfcDistributionFlowElement,
            {
                
            }
        );
        schema.define(
            "IfcDistributionControlElement",
            IfcDistributionElement,
            {
                {"ControlElementId"},
                
            }
        );
        schema.define(
            "IfcElectricDistributionPoint",
            IfcFlowController,
            {
                {"DistributionPointFunction"},
                {"UserDefinedFunction"},
                
            }
        );
        schema.define(
            "IfcReinforcingBar",
            IfcReinforcingElement,
            {
                {"NominalDiameter"},
                {"CrossSectionArea"},
                {"BarLength"},
                {"BarRole"},
                {"BarSurface"},
                
            }
        );
    }

    return schema;
}
