#include "simpleifc/core.h"

using namespace simpleifc;



simpleifc::CoreObject simpleifc::CoreClass::instance() const
{
    CoreObject obj(this);
    populate(obj);
    return obj;
}

void simpleifc::CoreClass::populate(simpleifc::CoreObject& obj) const
{
    if ( _parent )
        _parent->populate(obj);

    for ( const auto& prop : _properties )
        obj._properties.emplace_back(prop);
}



const PropertyValue& CoreObject::operator[](const std::string& prop) const
{
    for ( const auto& p : _properties )
        if ( p.name == prop )
            return p.value;
    return PropertyValue::undefined();
}

PropertyValue& CoreObject::operator[](const std::string& prop)
{
    for ( auto& p : _properties )
        if ( p.name == prop )
            return p.value;
    return PropertyValue::undefined();
}

const PropertyValue& CoreObject::operator[](int index) const
{
    return _properties[index].value;
}

PropertyValue& CoreObject::operator[](int index)
{
    return _properties[index].value;
}

const std::string& CoreObject::file_id() const
{
    return _file_id;
}

void CoreObject::set_file_id(const std::string& fid)
{
    _file_id = fid;
}

bool CoreObject::is_instance(const CoreClass* cls)
{
    if ( !cls )

    for ( const CoreClass* sc = _class; sc; sc = sc->parent() )
    {
        if ( sc == cls )
            return true;
    }
    return false;
}

const CoreClass* CoreObject::core_class() const
{
    return _class;
}

const std::string& CoreObject::classname() const
{
    return _class->name();
}

const std::vector<Property>& CoreObject::properties() const
{
    return _properties;
}

CoreObject& CoreObject::set(const std::string& prop_name, PropertyValue v)
{
    for ( auto& p : _properties )
        if ( p.name == prop_name )
        {
            p.value = std::move(v);
            break;
        }
    return *this;
}

CoreObject::CoreObject(const CoreClass* _class)
    : _class(_class)
{}
