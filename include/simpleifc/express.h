#pragma once

#include <unordered_map>
#include "simpleifc/core.h"

namespace simpleifc {

class Schema
{
public:
    explicit Schema(std::string name)
        : _name(std::move(name))
    {}

    CoreClass* define(std::string name, const CoreClass* parent, std::vector<Property> properties)
    {
        auto it = _classes.emplace(name, CoreClass(name, parent, std::move(properties))).first;
        return &it->second;
    }

    CoreClass* define(std::string name, std::vector<Property> properties)
    {
        return define(std::move(name), nullptr, std::move(properties));
    }

    const CoreClass* find(const std::string& name) const
    {
        auto it = _classes.find(name);
        if ( it == _classes.end() )
            return nullptr;
        return &it->second;
    }

    template<class... Args>
    CoreObject instance(const std::string& class_name, Args&&... args) const
    {
        auto cls = find(class_name);
        if ( !cls )
            throw IfcError("No class " + class_name + " in schema " + _name);
        CoreObject obj = cls->instance();
        obj.assign(std::forward<Args>(args)...);
        return obj;
    }

    const std::string& name() const
    {
        return _name;
    }

private:
    std::string _name;
    std::unordered_map<std::string, CoreClass> _classes;
};

} // namespace simpleifc
