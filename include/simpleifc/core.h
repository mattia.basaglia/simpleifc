#pragma once

#include <variant>
#include <string>
#include <memory>
#include <vector>
#include <stdexcept>

namespace simpleifc {

class IfcError : public std::logic_error
{
public:
    using std::logic_error::logic_error;
};

enum class BasicType
{
    String,
    Object,
    Reference,
    Integer,
    Real,
    Null,
    Enum,
    List
};

class PropertyValue;

class EnumValue
{
public:
    EnumValue(std::string value) : value(std::move(value)) {}

    std::string value;
};

using PropertyList = std::vector<PropertyValue>;


class CoreClass;
class PropertyValue;
class Property;

class CoreObject
{
public:
    const PropertyValue& operator[](const std::string& prop) const;

    PropertyValue& operator[](const std::string& prop);

    const PropertyValue& operator[](int index) const;

    PropertyValue& operator[](int index);

    const std::string& file_id() const;

    void set_file_id(const std::string& fid);

    bool is_instance(const CoreClass* cls);

    const CoreClass* core_class() const;

    const std::string& classname() const;

    const std::vector<Property>& properties() const;

    CoreObject& set(const std::string& prop_name, PropertyValue v);

    template<class... Args>
    void assign(Args&&... args)
    {
        on_assign(0, std::forward<Args>(args)...);
    }

    void assign(){}

private:
    CoreObject(const CoreClass* _class);

    template<class Head, class... Args>
    void on_assign(std::size_t index, Head&& h, Args&&... tail)
    {
        if ( index >= _properties.size() )
            throw IfcError("Property index " + std::to_string(index) + " out of bounds in " + classname());
        do_assign(index, std::forward<Head>(h));
        on_assign(index+1, std::forward<Args>(tail)...);
    }
    void on_assign(std::size_t index)
    {
        if ( index < _properties.size() )
            throw IfcError(
                "Too few arguments for " + classname() +
                " (got " + std::to_string(index) + ", expected "
                + std::to_string(_properties.size()) + ")"
            );
    }
    template<class Head>
    void do_assign(std::size_t index, Head&& h);

    friend CoreClass;

    const CoreClass* _class;
    std::vector<Property> _properties;
    std::string _file_id;
};

namespace detail {
    template<BasicType>
        struct _type_from_enum {};

    template<class T>
        struct _enum_from_type {};


#define TYPE_ENUM(enum_v,type_t) \
    template<> struct _type_from_enum<enum_v> { using type = type_t; }; \
    template<> struct _enum_from_type<type_t> { \
        using type = type_t; \
        static constexpr const BasicType value = enum_v; \
    }; \
    type_t _overload(type_t);

    TYPE_ENUM(BasicType::String, std::string)
    TYPE_ENUM(BasicType::Object, CoreObject)
    TYPE_ENUM(BasicType::Reference, CoreObject*)
    TYPE_ENUM(BasicType::Integer, int64_t)
    TYPE_ENUM(BasicType::Real, double)
    TYPE_ENUM(BasicType::Enum, EnumValue)
    TYPE_ENUM(BasicType::List, PropertyList)

    int64_t _overload(int);

#undef TYPE_ENUM

    template<BasicType v>
        using type_from_enum = typename detail::_type_from_enum<v>::type;

    template<class T>
        using closest_type = decltype(_overload(std::declval<T>()));

    template<class T>
        BasicType static constexpr const enum_from_type = detail::_enum_from_type<closest_type<T>>::value;


    template <class T, BasicType=detail::_enum_from_type<std::decay_t<T>>::value>
    constexpr T&& forward(std::remove_reference_t<T>& v) noexcept
    {
        return static_cast<T&&>(v);
    }
    template <class T, BasicType=detail::_enum_from_type<std::decay_t<T>>::value>
    constexpr T&& forward(std::remove_reference_t<T>&& v) noexcept
    {
        return static_cast<T&&>(v);
    }

    template<class T, class =
        std::enable_if_t<
            !std::is_same_v<
                std::decay_t<T>,
                std::decay_t<closest_type<T>>
            >
        >
    >
    closest_type<T> forward(const T& v)
    {
        return closest_type<T>(v);
    }

} // namespace



class PropertyValue
{
public:
    static PropertyValue& undefined()
    {
        static PropertyValue instance;
        return instance;
    }

    PropertyValue()
        : _type(BasicType::Null)
    {}

    PropertyValue(const PropertyValue&) = default;

    PropertyValue(PropertyValue& o) : PropertyValue((const PropertyValue&) o) {}

    PropertyValue(PropertyValue&&) = default;

    PropertyValue& operator=(const PropertyValue&) = default;

    PropertyValue& operator=(PropertyValue&&) = default;

    template<class T>
    PropertyValue(T&& v)
        : _value(detail::forward<T>(v)), _type(detail::enum_from_type<T>)
    {}

    PropertyValue(std::nullptr_t)
        : _type(BasicType::Null)
    {}

    template<class Visitor>
    void visit(Visitor& vis) const
    {
        switch ( _type )
        {
            case BasicType::String:
                vis(get<BasicType::String>());
                break;
            case BasicType::Object:
                vis(const_cast<CoreObject*>(&get<BasicType::Object>()));
                break;
            case BasicType::Reference:
                vis(get<BasicType::Reference>());
                break;
            case BasicType::Integer:
                vis(get<BasicType::Integer>());
                break;
            case BasicType::Real:
                vis(get<BasicType::Real>());
                break;
            case BasicType::Null:
                vis(nullptr);
                break;
            case BasicType::Enum:
                vis(get<BasicType::Enum>());
                break;
            case BasicType::List:
                vis(get<BasicType::List>());
                break;
        };
    }

private:
    template<BasicType b>
    const detail::type_from_enum<b>& get() const
    {
        return std::get<detail::type_from_enum<b>>(_value);
    }

    std::variant<
        detail::type_from_enum<BasicType::String>,
        detail::type_from_enum<BasicType::Object>,
        detail::type_from_enum<BasicType::Reference>,
        detail::type_from_enum<BasicType::Integer>,
        detail::type_from_enum<BasicType::Real>,
        detail::type_from_enum<BasicType::Enum>,
        detail::type_from_enum<BasicType::List>
    > _value;
    BasicType _type = BasicType::Null;
};

class Property
{
public:
    Property(std::string name, PropertyValue value={})
        : name(std::move(name)), value(std::move(value))
    {}

    std::string name;
    PropertyValue value;
};

class CoreClass
{
public:
    CoreClass(std::string name, const CoreClass* parent, std::vector<Property> properties)
        : _name(std::move(name)), _parent(parent), _properties(std::move(properties))
    {}

    CoreObject instance() const;

    const CoreClass* parent() const
    {
        return _parent;
    }

    const std::string& name() const
    {
        return _name;
    }

private:
    void populate(CoreObject& obj) const;

    std::string _name;
    const CoreClass* _parent;
    std::vector<Property> _properties;
};


template<class Head>
void CoreObject::do_assign(std::size_t index, Head&& h)
{
    _properties[index].value = std::forward<Head>(h);
}


} // namespace simpleifc
