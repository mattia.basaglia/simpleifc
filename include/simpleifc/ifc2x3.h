#pragma once

#include "simpleifc/express.h"

namespace simpleifc {
namespace ifc2x3 {

const Schema& schema();

} // namespace ifc2x3
} // namespace simpleifc
