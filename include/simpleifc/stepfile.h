#pragma once

#include <fstream>
#include <deque>
#include <cctype>
#include "simpleifc/express.h"

namespace simpleifc {

class StepWriter
{
public:
    static const Schema& header_schema()
    {
        static Schema schema("");
        static bool initialized = false;
        if ( !initialized )
        {
            initialized = true;
            schema.define(
                "file_description",
                {
                    {"description", ""},
                    {"implementation_level", "2;1"}
                }
            );
            schema.define(
                "file_name",
                {
                    {"name", ""},
                    {"time_stamp", ""},
                    {"author", PropertyList{""}},
                    {"organization", PropertyList{""}},
                    {"preprocessor_version", " "},
                    {"originating_system", "GeoBim"},
                    {"authorization", " "},
                }
            );
            schema.define(
                "file_schema",
                {
                    {"schema"}
                }
            );
        }
        return schema;
    }

    explicit StepWriter(std::ostream *stream)
        : _stream(stream)
    {
        _stream->setf(std::ios_base::showpoint);
    }

    void write_header(const std::string& filename, const Schema& schema)
    {
        *_stream << "ISO-10303-21;\n";
        *_stream << "HEADER;\n";
        CoreObject desc = header_schema().instance("file_description");
        CoreObject name = header_schema().instance("file_name");
        CoreObject fschema = header_schema().instance("file_schema");
        name["name"] = filename;
        fschema["schema"] = PropertyList{schema.name()};
        write_header(desc);
        write_header(name);
        write_header(fschema);

        *_stream << "ENDSEC;\nDATA;\n";
    }

    void write_footer()
    {
//         clean_queue();
        *_stream << "ENDSEC;\n";
        *_stream << "END-ISO-10303-21;\n";
    }

    void operator()(const std::string& str)
    {
        _stream->put('\'');
        for ( char c : str )
        {
            if ( c != '\'' && c < 127 )
                _stream->put(c);
        }
        _stream->put('\'');
    }

    void operator()(CoreObject* obj)
    {
//         if ( obj->properties().size() == 1 )
//             write_object(obj);
//         else
            *_stream << object_id(obj);
    }

    void operator()(int64_t v)
    {
        *_stream << v;
    }

    void operator()(double v)
    {
        *_stream << v;
    }

    void operator()(std::nullptr_t)
    {
        _stream->put('$');
    }

    void operator()(const EnumValue& v)
    {
        if ( v.value == "*" )
        {
            _stream->put('*');
            return;
        }

        _stream->put('.');
        for ( char c : v.value )
            _stream->put(std::toupper(c));
        _stream->put('.');
    }

    void operator()(const std::vector<PropertyValue>& v)
    {
        _stream->put('(');
        for ( std::size_t i = 0; i < v.size(); i++ )
        {
            if ( i != 0 )
                _stream->put(',');

            v[i].visit(*this);
        }
        _stream->put(')');
    }

    void define_object(CoreObject* obj)
    {
        pre_write(*obj);

        if ( obj->file_id().empty() )
            obj->set_file_id(generate_id());

        *_stream << obj->file_id() << '=';
        write_object(obj);
        _stream->put(';');
        _stream->put('\n');

//         clean_queue();
    }

    void define_object(CoreObject& obj)
    {
        return define_object(&obj);
    }

    void define_object(CoreObject&& obj)
    {
        return define_object(&obj);
    }

    void define_object(const CoreObject& obj)
    {
        pre_write(obj);
        std::string id = obj.file_id().empty() ? generate_id() : obj.file_id();

        *_stream << id << "= ";
        write_object(&obj);
        _stream->put(';');
        _stream->put('\n');

//         clean_queue();
    }

private:
//     void clean_queue()
//     {
//         std::vector<CoreObject*> objq;
//         std::swap(objq, _queue);
//         for ( CoreObject* child : objq )
//             define_object(child);
//     }

    void write_header(const CoreObject& obj)
    {
        write_object(&obj);
        _stream->put(';');
        _stream->put('\n');
    }

    void write_object(const CoreObject* obj)
    {
        for ( char c : obj->classname() )
            _stream->put(std::toupper(c));
        _stream->put('(');
        for ( std::size_t i = 0; i < obj->properties().size(); i++ )
        {
            if ( i != 0 )
                _stream->put(',');

            obj->properties()[i].value.visit(*this);
        }
        _stream->put(')');
    }

    const std::string& object_id(CoreObject* obj)
    {
        if ( obj->file_id().empty() )
        {
            throw IfcError("IFC Entity without ID " + obj->classname());
//             _queue.push_back(obj);
//             obj->set_file_id(generate_id());
        }
        return obj->file_id();
    }

    class PreWrite
    {
    public:
        void operator()(CoreObject* obj)
        {
            if ( /*obj->properties().size() > 1 &&*/ obj->file_id().empty() )
            {
                writer->define_object(obj);
            }
        }

        void operator()(const std::vector<PropertyValue>& l)
        {
            for ( const auto& v : l )
                v.visit(*this);
        }

        template<class T>
        void operator()(T){}

        void operator()(std::nullptr_t){}

        StepWriter* writer;
    };


    void pre_write(const CoreObject& obj)
    {
        PreWrite pw{this};

        for ( const Property& prop : obj.properties() )
        {
            prop.value.visit(pw);
        }
    }

    std::string generate_id()
    {
        return "#" + std::to_string(++_id);
    }

    std::ostream *_stream;
    int _id = 0;
//     std::vector<CoreObject*> _queue;
};


} // namespace simpleifc
