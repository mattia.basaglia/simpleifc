#!/usr/bin/env python3
import os
import argparse
from express.parser import Parser
from express import generators
from express.generator import Generator
import pkgutil
import inspect
import importlib


here = os.path.dirname(os.path.abspath(__file__))
available_generators = {
    mod.name: next((
        cls
        for name, cls in inspect.getmembers(importlib.import_module("express.generators.%s" % mod.name))
        if inspect.isclass(cls) and issubclass(cls, Generator) and cls is not Generator
    ))

    for mod in pkgutil.iter_modules(generators.__path__)
}

argparser = argparse.ArgumentParser(
    description="Generates code from EXPRESS schema files"
)
argparser.add_argument(
    "input",
    nargs="?",
    help="Express schema to parse. See https://technical.buildingsmart.org/standards/ifc/ifc-schema-specifications/",
    default="ifc2x3.exp"
)
argparser.add_argument(
    "--output", "-o",
    help="Output path",
    default="generated",
)
argparser.add_argument(
    "--generator", "-g",
    help="Generator to use",
    default="print",
    choices=available_generators.keys()
)
argparser.add_argument(
    "--template-path", "-t",
    help="Jinja template path",
    default=os.path.join(here, "express", "templates"),
)
ns = argparser.parse_args()
schema_dir = os.path.join(here, "schemas", "")
infile = os.path.join(schema_dir, ns.input)

parser = Parser(open(infile, "rb"))
parser.parse()
genclass = available_generators[ns.generator]
gen = genclass(ns.output, ns.template_path, ns.input)
gen.process_schema(parser.schema)
