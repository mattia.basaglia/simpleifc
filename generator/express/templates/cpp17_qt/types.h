{% include "cpp17_qt/boilerplate.h" %}
#ifndef {{ generator.schema_ns|upper }}_TYPES_H
#define {{ generator.schema_ns|upper }}_TYPES_H

#include "ifc/base/primitives.h"

namespace ifc::{{ generator.schema_ns }} {

{% for type in types %}
{% if type|is_enum -%}
enum class {{ type.name }}
{
    {%- for value in type.base.values %}
    {{ value }}{%- if value == "NULL" -%}_{%- endif %},
    {%- endfor %}
};
{%- else -%}
using {{ type.name }} = {{ type.base|render_type }};
{%- endif -%}
{%- endfor %}

} // namespace ifc::{{ generator.schema_ns }}

#endif // {{ generator.schema_ns|upper }}TYPES_H
