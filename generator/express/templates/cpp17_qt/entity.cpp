{% include "cpp17_qt/boilerplate.h" %}
#include "{{ header }}"

{% for dep in soft_deps -%}
    {{ dep }}
{% endfor %}

using namespace ifc::{{ generator.schema_ns }};

class ifc::{{ generator.schema_ns }}::{{ entity.name }}Private
{
public:
{% for attr in entity.attributes %}
    {{ attr.type|render_type }} {{ attr.name }};
{%- endfor %}
};


{{ entity.name }}::{{ entity.name }}()
    : d(std::make_unique<{{ entity.name }}Private>())
{}

{{ entity.name }}::~{{ entity.name }}() = default;

