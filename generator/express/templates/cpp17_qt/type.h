{% include "cpp17_qt/boilerplate.h" %}
#ifndef {{ generator.schema_ns|upper }}_{{ type.name|upper }}_H
#define {{ generator.schema_ns|upper }}_{{ type.name|upper }}_H

{% for dep in type.dependencies() -%}
{{ dep|include_dep }}
{% endfor %}

namespace ifc::{{ generator.schema_ns }} {

using {{ type.name }} = {{ type.base|render_type }};

} // namespace ifc::{{ generator.schema_ns }}

#endif // {{ generator.schema_ns|upper }}_{{ type.name|upper }}_H

