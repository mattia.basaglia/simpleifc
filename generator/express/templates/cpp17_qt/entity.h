{% include "cpp17_qt/boilerplate.h" %}
#ifndef {{ generator.schema_ns|upper }}_{{ entity.name|upper }}_H
#define {{ generator.schema_ns|upper }}_{{ entity.name|upper }}_H

{% if not entity.dependencies() -%}
#include "ifc/base/primitives.h"
{% else -%}
{% for dep in entity.dependencies() -%}
{{ dep|include_dep }}
{% endfor -%}
{%- endif %}

namespace ifc::{{ generator.schema_ns }} {

class {{ entity.name }}Private;

class {{ entity.name }} : {% if entity.bases -%}
    {%- for base in entity.bases -%}
        public {{ base|render_type}}
        {%- if not loop.last -%}
            ,
        {%- endif -%}
    {%- endfor -%}
{%- else -%}
    public {{ baseclass }}
{%- endif %}
{
    Q_OBJECT

public:
    {{ entity.name }}();
    ~{{ entity.name }}();

private:
    std::unique_ptr<{{ entity.name }}Private> d;
};

} // namespace ifc::{{ generator.schema_ns }}

#endif // {{ generator.schema_ns|upper }}_{{ entity.name|upper }}_H

