from jinja2 import Environment, FileSystemLoader
from . import types
import datetime


class Generator:
    def __init__(self, output_path, template_path, schema_filename):
        self.output_path = output_path
        self.jinja = Environment(
            loader=FileSystemLoader([template_path])
        )
        self.schema_filename = schema_filename
        self.generation_date = datetime.datetime.now()
        self.initialize()

    def initialize(self):
        pass

    def render_template(self, template_name, **kwargs):
        template = self.jinja.get_template(template_name)
        kwargs.setdefault("generator", self)
        return template.render(**kwargs)

    def process_schema(self, schema: types.IfcSchema):
        self.start_schema(schema)

        self.decl_dict = {}
        for decl in schema.declarations:
            self.decl_dict[decl.provides()] = decl

        ready = set()
        left = list(schema.declarations)
        while left:
            sz = len(left)
            i = 0
            while i < len(left):
                if left[i].dependencies() - ready:
                    i += 1
                else:
                    self.process_declaration(left[i])
                    ready.add(left[i].provides())
                    del left[i]
            if len(left) == sz:
                raise Exception("Couldn't satisfy dependencies for:\n%s" % (
                    "\n".join(" * %s %s" % (d.__class__.__name__, d.provides()))
                ))

        self.end_schema()

    def start_schema(self, schema: types.IfcSchema):
        raise NotImplementedError

    def end_schema(self):
        pass

    def process_declaration(self, decl):
        self.visit(decl)

    def visit(self, o):
        return getattr(self, o.__class__.__name__)(o)

