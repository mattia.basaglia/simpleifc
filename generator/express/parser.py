import enum
import os

from .types import *


class TokenType(enum.Enum):
    EOF = enum.auto()
    Unknown = enum.auto()
    Label = enum.auto()
    Operator = enum.auto()
    String = enum.auto()
    Number = enum.auto()


class Token:
    def __init__(self, type, value):
        self.type = type
        self.value = value
        self.pos = None

    @property
    def lcvalue(self):
        return self.value.lower()

    def __repr__(self):
        return "<Token %s %s>" % (self.type.name, self.value)

    def match(self, type, value=None):
        return self.type == type and (value is None or value.lower() == self.lcvalue)


class Lexer:
    def __init__(self, file):
        self.file = file
        self.token = Token(TokenType.Unknown, None)
        self.file.seek(0, os.SEEK_END)
        self.file_size = self.file.tell()
        self.file.seek(0)
        self.line = 1
        self.column = 0

    @property
    def filename(self):
        return getattr(self.file, "name", "stream")

    def _unget(self):
        if self.file.tell() <= self.file_size and self.file.tell() > 0:
            self.file.seek(self.file.tell() - 1)
            self.column -= 1
            if self.column < 0:
                self.line -= 1

    def _get(self):
        c = self.file.read(1).decode("ascii")
        if c == '\n':
            self.line += 1
            self.column = 0
        elif c != '':
            self.column += 1
        return c

    def _find_next_token(self):
        c = ' '
        while True:
            while c.isspace():
                c = self._get()

            if c == '(':
                if self._get() == '*':
                    while True:
                        d = self._get()
                        if c == '*' and d == ')':
                            c = ' '
                            break
                        if d == '':
                            return ''
                        c = d
                else:
                    self._unget()
                    break
            else:
                break
        return c

    def __iter__(self):
        return self

    def __next__(self):
        if self.token and self.token.type == TokenType.EOF:
            raise StopIteration
        return self.next()

    def next(self):
        c = self._find_next_token()
        pos = (self.line, self.column)
        if c == '':
            self.token = Token(TokenType.EOF, '')
        elif c.isalpha():
            self.token = self._label(c)
        elif c.isdigit():
            self.token = self._number(c)
        elif c == "'":
            self.token = self._string()
        elif c in "+-:{};[]()=.,*/\\|?":
            self.token = Token(TokenType.Operator, c)
        elif c in "<>":
            d = self._get()
            if d == "=":
                self.token = Token(TokenType.Operator, c+d)
            else:
                self._unget()
                self.token = Token(TokenType.Operator, c)
        else:
            self.token = Token(TokenType.Unknown, c)
        self.token.pos = pos
        return self.token

    def _label(self, c):
        label = ""
        while c.isalnum() or c == "_":
            label += c
            c = self._get()
        self._unget()
        return Token(TokenType.Label, label)

    def _number(self, c):
        label = ""
        while c.isdigit():
            label += c
            c = self._get()
        self._unget()
        return Token(TokenType.Number, label)

    def _string(self):
        label = ""
        c = ' '
        while c != "'" and c != '':
            label += c
            c = self._get()
        return Token(TokenType.String, label[1:])


class Parser:
    def __init__(self, file):
        self.lexer = Lexer(file)
        self.schema = None

    def parse(self):
        self._next()
        self._parse_schema()
        return self.schema

    def _parse_schema(self):
        self._expect(TokenType.Label, "schema")
        self._nextpect(TokenType.Label)
        name = self.lexer.token.value
        self.schema = IfcSchema(name)
        self._nextpect(TokenType.Operator, ";")
        self._next()
        self._parse_declarations()
        self._expect(TokenType.Label, "end_schema")
        self._nextpect(TokenType.Operator, ";")
        self._nextpect(TokenType.EOF)

    def _match(self, type, value=None):
        return self.lexer.token.match(type, value)

    def _expect(self, type, value=None):
        if not self._match(type, value):
            self._error("Expected %s%s" % (
                type.name,
                " %r" % value if value is not None else ""
            ))

    def _error(self, message):
        raise Exception("%s:%s:%s: %s got %s %r" % (
            self.lexer.filename,
            self.lexer.token.pos[0],
            self.lexer.token.pos[1],
            message,
            self.lexer.token.type.name,
            self.lexer.token.value
        ))

    def _next(self):
        return self.lexer.next()

    def _has_label(self, label):
        return self._match(TokenType.Label, label)

    def _eof(self):
        return self._match(TokenType.EOF)

    def _nextpect(self, type, value=None):
        self._next()
        self._expect(type, value)

    def _parse_declarations(self):
        while not self._eof():
            if self._has_label("type"):
                self._parse_type_decl()
            elif self._has_label("entity"):
                self._parse_entity_decl()
            elif self._has_label("function"):
                self._parse_function_decl()
            elif self._has_label("rule"):
                self._parse_rule_decl()
            elif self._has_label("end_schema"):
                break
            else:
                self._error("Expected declaration")

    def _parse_type_decl(self):
        self._nextpect(TokenType.Label)
        name = self.lexer.token.value
        self._nextpect(TokenType.Operator, "=")
        self._next()
        base = self._parse_underlying_type()
        self._expect(TokenType.Operator, ";")
        self.schema.declarations.append(IfcType(name, base))
        self._next()
        self.schema.declarations[-1].constraints = self._parse_where_clause()
        self._expect(TokenType.Label, "end_type")
        self._nextpect(TokenType.Operator, ";")
        self._next()

    def _parse_where_clause(self):
        constraints = []
        if self._has_label("where"):
            self._next()
            while not (self._match(TokenType.Label) and self.lexer.token.lcvalue.startswith("end_")):
                self._expect(TokenType.Label)
                self._nextpect(TokenType.Operator, ":")
                self._next()
                constraints.append(self._parse_expression())
                self._expect(TokenType.Operator, ";")
                self._next()
        return constraints

    def _parse_underlying_type(self):
        self._expect(TokenType.Label)
        typename = self.lexer.token.value
        typename_l = self.lexer.token.lcvalue
        self._next()
        if typename_l in ["array", "list", "set"]:
            return self._parse_type_array(typename_l)
        elif typename_l in ["binary",  "string"]:
            return self._parse_type_string(typename_l)
        elif typename_l in ["boolean", "integer", "logical", "number", "real"]:
            return IfcPrimitiveType(typename_l)
        elif typename_l == "enumeration":
            return self._parse_type_enumeration()
        elif typename_l == "select":
            return self._parse_type_select()
        else:
            return IfcDefinedType(typename)

    def _parse_type_string(self, typename_l):
        width = None
        fixed = False
        if self._match(TokenType.Operator, "("):
            self._next()
            width = self._parse_numeric_expression()
            self._expect(TokenType.Operator, ")")
            self._next()
            if self._has_label("fixed"):
                fixed = True
                self._next()
        return IfcPrimitiveString(typename_l, width, fixed)

    def _parse_type_select(self):
        items = self._parse_enumeration_items()
        return IfcSelect(list(map(IfcDefinedType, items)))

    def _parse_type_enumeration(self):
        self._expect(TokenType.Label, "of")
        self._next()
        items = self._parse_enumeration_items()
        return IfcEnumeration(items)

    def _parse_enumeration_items(self):
        self._expect(TokenType.Operator, "(")
        labels = []
        while True:
            self._nextpect(TokenType.Label)
            labels.append(self.lexer.token.value)
            self._nextpect(TokenType.Operator)
            if self.lexer.token.value == ")":
                self._next()
                return labels
            self._expect(TokenType.Operator, ",")

    def _parse_type_array(self, array_type):
        self._expect(TokenType.Operator, "[")
        self._next()
        minv = self._parse_array_bounds()
        self._expect(TokenType.Operator, ":")
        self._next()
        maxv = self._parse_array_bounds()
        self._expect(TokenType.Operator, "]")
        self._nextpect(TokenType.Label, "of")
        self._next()
        if self._has_label("unique"):
            self._next()
        type = self._parse_instantiable_type()
        return IfcArray(array_type, type, minv, maxv)

    def _parse_instantiable_type(self):
        return self._parse_underlying_type()

    def _parse_array_bounds(self):
        if self._match(TokenType.Number):
            return self._parse_numeric_expression()
        self._expect(TokenType.Operator, "?")
        self._next()
        return None

    def _parse_expression(self):
        tokens = []
        while not self._match(TokenType.Operator, ";") and not self._eof():
            tokens.append(self.lexer.token)
            self._next()
        return IfcExpression(tokens)

    def _parse_numeric_expression(self):
        self._expect(TokenType.Number)
        v = int(self.lexer.token.value)
        self._next()
        return v

    def _parse_todo_skip(self, match):
        while not self._match(TokenType.Label) or self.lexer.token.lcvalue not in match:
            self._next()

    def _parse_entity_decl(self):
        self._nextpect(TokenType.Label)
        name = self.lexer.token.value
        self._next()
        self._parse_supertype_constraint()
        parents = self._subtype_declaration()
        self._expect(TokenType.Operator, ";")
        self.schema.declarations.append(IfcEntity(name, parents))
        self._next()

        sections = {"derive", "inverse", "unique", "where", "end_entity"}
        while True:
            self._expect(TokenType.Label)
            if self.lexer.token.lcvalue in sections:
                break
            self.schema.declarations[-1].attributes.append(self._parse_attribute())
            self._expect(TokenType.Operator, ";")
            self._next()

        sections.remove("derive")
        if self._has_label("derive"):
            self._next()
            self._parse_todo_skip(sections)

        sections.remove("inverse")
        if self._has_label("inverse"):
            self._next()
            self._parse_todo_skip(sections)

        sections.remove("unique")
        if self._has_label("unique"):
            self._next()
            self._parse_todo_skip(sections)

        self.schema.declarations[-1].constraints = self._parse_where_clause()

        self._expect(TokenType.Label, "end_entity")
        self._nextpect(TokenType.Operator, ";")
        self._next()

    def _parse_attribute(self):
        name = self.lexer.token.value
        self._next()
        self._expect(TokenType.Operator, ":")
        self._next()
        optional = False
        if self._has_label("optional"):
            optional = True
            self._next()
        type = self._parse_instantiable_type()
        if optional:
            type = IfcOptional(type)
        return IfcAttribute(name, type)

    def _subtype_declaration(self):
        if not self._has_label("subtype"):
            return []
        self._nextpect(TokenType.Label, "of")
        self._next()
        items = self._parse_enumeration_items()
        return list(map(IfcDefinedEntity, items))

    def _parse_supertype_constraint(self):
        abstract = False
        if self._has_label("abstract"):
            self._next()
            abstract = True

        if self._has_label("supertype"):
            self._next()
            if self._has_label("of") or not abstract:
                self._parse_subtype_constraint()
        return abstract

    def _parse_subtype_constraint(self):
        self._expect(TokenType.Label, "of")
        self._nextpect(TokenType.Operator, "(")
        self._next()
        self._parse_supertype_expression()
        self._expect(TokenType.Operator, ")")
        self._next()

    def _parse_supertype_expression(self):
        while True:
            self._parse_supertype_factor()
            if self._match(TokenType.Label, "andor"):
                self._next()
            else:
                break

    def _parse_supertype_factor(self):
        while True:
            self._parse_supertype_term()
            if self._match(TokenType.Label, "and"):
                self._next()
            else:
                break

    def _parse_supertype_term(self):
        if self._match(TokenType.Label, "oneof"):
            self._nextpect(TokenType.Operator, "(")
            self._next()
            while True:
                self._parse_supertype_expression()
                if self._match(TokenType.Operator, ")"):
                    self._next()
                    return
                else:
                    self._expect(TokenType.Operator, ",")
                    self._next()
        elif self._match(TokenType.Label, "("):
            self._parse_supertype_expression()
            self._expect(TokenType.Operator, ")")
            self._next()
        else:
            self._expect(TokenType.Label)
            self._next()

    def _parse_function_decl(self):
        self._next()
        self._expect(TokenType.Label)
        name = self.lexer.token.value
        self._parse_todo_skip({"end_function"})
        self._expect(TokenType.Label, "end_function")
        self._nextpect(TokenType.Operator, ";")
        self._next()

    def _parse_rule_decl(self):
        self._next()
        self._expect(TokenType.Label)
        name = self.lexer.token.value
        self._nextpect(TokenType.Label, "for")
        self._parse_todo_skip({"end_rule"})
        self._expect(TokenType.Label, "end_rule")
        self._nextpect(TokenType.Operator, ";")
        self._next()
