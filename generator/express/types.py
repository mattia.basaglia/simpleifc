class IfcSchema:
    def __init__(self, name):
        self.name = name
        self.declarations = []


class IfcSelect:
    def __init__(self, values):
        self.values = values

    def dependencies(self):
        deps = set()
        for v in self.values:
            deps |= v.dependencies()
        return deps


class IfcEnumeration:
    def __init__(self, values):
        self.values = values

    def dependencies(self):
        return set()


class IfcPrimitiveString:
    def __init__(self, type, width, fixed):
        self.type = type
        self.width = width
        self.fixed = fixed

    def dependencies(self):
        return set()


class IfcPrimitiveType:
    def __init__(self, type):
        self.type = type

    def dependencies(self):
        return set()


class IfcDefinedType:
    def __init__(self, type):
        self.type = type

    def dependencies(self):
        return {self.type}


class IfcArray:
    def __init__(self, array_type, type, min_size, max_size):
        self.array_type = array_type
        self.type = type
        self.min_size = min_size
        self.max_size = max_size

    def dependencies(self):
        return self.type.dependencies()


class IfcType:
    def __init__(self, name, base):
        self.name = name
        self.base = base
        self.constraints = []

    def dependencies(self):
        return self.base.dependencies()

    def provides(self):
        return self.name


class IfcExpression:
    def __init__(self, tokens):
        self.tokens = tokens


class IfcDefinedEntity:
    def __init__(self, type):
        self.type = type

    def dependencies(self):
        return {self.type}


class IfcEntity:
    def __init__(self, name, bases):
        self.name = name
        self.bases = bases
        self.constraints = []
        self.attributes = []

    def dependencies(self):
        deps = set()
        for v in self.bases:
            deps |= v.dependencies()
        return deps

    def provides(self):
        return self.name


class IfcOptional:
    def __init__(self, type):
        self.type = type

    def dependencies(self):
        return self.type.dependencies()


class IfcAttribute:
    def __init__(self, name, type):
        self.name = name
        self.type = type

    def dependencies(self):
        return self.type.dependencies()
