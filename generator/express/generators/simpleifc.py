import os
from ..generator import Generator
from .. import types


class SimpleIfcGenerator(Generator):
    def start_schema(self, o: types.IfcSchema):
        self.schema_ns = o.name.lower()

        with open(os.path.join(self.output_path, self.schema_ns + ".h"), "w") as header:
            header.write(
                self.render_template(
                    "simpleifc/header",
                    schema_ns=self.schema_ns
                )
            )

        self.outfile = open(os.path.join(self.output_path, self.schema_ns + ".cpp"), "w")
        self.write(
            self.render_template(
                "simpleifc/cpp_start",
                schema_ns=self.schema_ns,
                schema_name=o.name,
            )
        )

        self.keep = set()
        for decl in o.declarations:
            if isinstance(decl, types.IfcEntity):
                if len(decl.bases) > 1:
                    raise Exception("too many bases")
                elif len(decl.bases) == 1:
                    self.keep.add(decl.bases[0].type)


    def end_schema(self):
        self.write(
            self.render_template(
                "simpleifc/cpp_end",
                schema_ns=self.schema_ns,
            )
        )
        self.outfile.close()

    def IfcType(self, o: types.IfcType):
        pass

    def IfcEntity(self, o: types.IfcEntity):
        print(o.name)
        ctx = {
            "entity": o,
            "keep": o.name in self.keep
        }
        self.write(
            self.render_template("simpleifc/entity", **ctx)
        )

    def write(self, str):
        self.outfile.write(str)
