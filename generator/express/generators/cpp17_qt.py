import os
from ..generator import Generator
from .. import types


class Cpp17QtGenerator(Generator):
    def _open_out_file(self, name):
        return open(os.path.join(self.schema_path, name), "w")

    def initialize(self):
        self.jinja.filters["include_dep"] = self.include_dep
        self.jinja.filters["render_type"] = self.visit
        self.jinja.filters["is_enum"] = lambda o: isinstance(o.base, types.IfcEnumeration)

    def is_basic(self, decl):
        visited = set()

        def gather_deps(ob):
            if isinstance(ob, types.IfcEntity):
                raise Exception
            visited.add(ob.name)
            ob_deps = set(ob.dependencies())
            for dep in ob_deps:
                if dep not in visited:
                    gather_deps(self.decl_dict[dep])

        try:
            gather_deps(decl)
        except Exception:
            return False
        return True

    def include_dep(self, dep):
        depobj = self.decl_dict[dep]
        if depobj in self.declared_types:
            header = "types.h"
        else:
            header = self.header_filename(depobj)

        return '#include "%s"' % header

    def start_schema(self, o: types.IfcSchema):
        self.schema_ns = o.name.lower()
        self.schema_path = os.path.join(self.output_path, self.schema_ns)
        os.makedirs(self.schema_path, exist_ok=True)
        self.declared_types = []
        self.cppfiles = []
        self.deferred = []

    def process_declaration(self, decl):
        if isinstance(decl, types.IfcType) and self.is_basic(decl):
            self.declared_types.append(decl)
        else:
            self.deferred.append(decl)

    def end_schema(self):
        for decl in self.deferred:
            self.visit(decl)

        with self._open_out_file("types.h") as types_file:
            types_file.write(self.render_template(
                "cpp17_qt/types.h",
                filename="types.h",
                types=self.declared_types
            ))

        with self._open_out_file("CMakeLists.txt") as cmake_file:
            cmake_file.write(self.render_template(
                "cpp17_qt/CMakeLists.txt",
                cppfiles=self.cppfiles
            ))

    def IfcType(self, o: types.IfcType):
        header_filename = self.header_filename(o)
        with self._open_out_file(header_filename) as header:
            header.write(self.render_template("cpp17_qt/type.h", type=o))

    def header_filename(self, o: types.IfcEntity):
        return "%s.h" % o.name.lower()

    def IfcEntity(self, o: types.IfcEntity):
        print(o.name)

        soft_deps = set()
        for attr in o.attributes:
            for dep in attr.dependencies():
                soft_deps.add(self.include_dep(dep))

        header_filename = self.header_filename(o)
        ctx = {
            "header": header_filename,
            "entity": o,
            "soft_deps": soft_deps,
            "baseclass": "IfcEntity"
        }
        with self._open_out_file(header_filename) as header:
            header.write(self.render_template("cpp17_qt/entity.h", **ctx))

        impl_filename = "%s.cpp" % o.name.lower()
        self.cppfiles.append(impl_filename)
        with self._open_out_file(impl_filename) as impl:
            impl.write(self.render_template("cpp17_qt/entity.cpp", **ctx))

    def IfcSelect(self, o: types.IfcSelect):
        return "std::variant<%s>" % ", ".join(map(self.visit, o.values))

    #def IfcEnumeration(self, o: types.IfcEnumeration):
        #return "enum\n{\n" + ",\n".join("    %s" % e for e in o.values) + "\n};"

    def IfcPrimitiveString(self, o: types.IfcPrimitiveString):
        # TODO fixed size?
        return o.type.title()

    def IfcPrimitiveType(self, o: types.IfcPrimitiveType):
        return o.type.title()

    def IfcDefinedType(self, o: types.IfcDefinedType):
        return o.type

    def IfcArray(self, o: types.IfcArray):
        #if o.array_type == "set":
            #array_type = "std::unordered_set"
        #else:
            #array_type = "std::vector"
        array_type = "std::vector"
        # TODO min/max size?
        return "%s<%s>" % (array_type, self.visit(o.type))

    #def IfcExpression(self, o: types.IfcExpression):
        #return " ".join(x.value for x in o.tokens)

    def IfcDefinedEntity(self, o: types.IfcDefinedEntity):
        return o.type

    def IfcOptional(self, o: types.IfcOptional):
        return "std::optional<%s>" % self.visit(o.type)

    #def IfcAttribute(self, o: types.IfcAttribute):
        #return "%s : %s" % (o.name, self.visit(o.type))
