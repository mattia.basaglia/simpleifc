from ..generator import Generator
from .. import types


class PrintGenerator(Generator):
    def start_schema(self, o):
        print("schema %s" % o.name)

    def process_declaration(self, decl):
        print(self.visit(decl)+"\n")

    def IfcSelect(self, o: types.IfcSelect):
        return "select\n" + "\n".join(" * %s" % e for e in o.values)

    def IfcEnumeration(self, o: types.IfcEnumeration):
        return "enum\n" + "\n".join(" * %s" % e for e in o.values)

    def IfcPrimitiveString(self, o: types.IfcPrimitiveString):
        return "%s%s%s" % (
            o.type,
            " [:%s]" % o.width if o.width is not None else "",
            " fixed" if o.fixed else ""
        )

    def IfcPrimitiveType(self, o: types.IfcPrimitiveType):
        return o.type

    def IfcDefinedType(self, o: types.IfcDefinedType):
        return o.type

    def IfcArray(self, o: types.IfcArray):
        return "%s<%s>[%s:%s]" % (o.array_type, self.visit(o.type), o.min_size, o.max_size)

    def IfcType(self, o: types.IfcType):
        string = "Type %s: %s" % (o.name, self.visit(o.base))
        if o.constraints:
            string += "\n  where:"
            for c in o.constraints:
                string += "\n    %s" % c
        return string

    def IfcExpression(self, o: types.IfcExpression):
        return " ".join(x.value for x in o.tokens)

    def IfcDefinedEntity(self, o: types.IfcDefinedEntity):
        return o.type

    def IfcEntity(self, o: types.IfcEntity):
        string = "Entity %s:" % (o.name)
        if o.bases:
            string += "\n  bases:"
            for c in o.bases:
                string += "\n    %s" % self.visit(c)
        if o.constraints:
            string += "\n  where:"
            for c in o.constraints:
                string += "\n    %s" % self.visit(c)
        if o.attributes:
            string += "\n  attrs:"
            for c in o.attributes:
                string += "\n    %s" % self.visit(c)
        return string

    def IfcOptional(self, o: types.IfcOptional):
        return "[%s]" % self.visit(o.type)

    def IfcAttribute(self, o: types.IfcAttribute):
        return "%s : %s" % (o.name, self.visit(o.type))
