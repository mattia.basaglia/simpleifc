from ..generator import Generator
from .. import types


class DependencyListGenerator(Generator):
    def start_schema(self, o):
        print("schema %s" % o.name)

    def process_declaration(self, decl):
        print("%s : %s" % (decl.provides(), " ".join(decl.dependencies())))
