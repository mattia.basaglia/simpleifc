SimpleIFC
=========

Simple library to handle IFC / Express / Step


Code generation
---------------

There are python scripts to generate code based on Express schemas.
The main script is `./generator/express_parser.py`


C++ Library
-----------

Instead of having a bunch of C++ classes and object there's a simple abstraction
layer and all IFC objects are instances of `simpleifc::CoreObject`.


### Instanciation

```c++
using namespace simpleifc;

const Schema& schema = ifc2x3::schema();

// Instanciate and initialize all properties
CoreObject direction_x = schema.instance("IfcDirection", PropertyList{1., 0., 0.});
CoreObject direction_y = schema.instance("IfcDirection", PropertyList{0., 1., 0.});
CoreObject direction_z = schema.instance("IfcDirection", PropertyList{0., 0., 1.});
CoreObject pt_origin = schema.instance("IfcCartesianPoint", PropertyList{0., 0., 0.});

CoreObject context = schema.instance("IfcGeometricRepresentationContext",
    nullptr, "Model", 3, 1e-5,
    schema.instance("IfcAxis2Placement3D", pt_origin, direction_z, direction_x),
    direction_y
);


// Instanciate and set properties by name
// single expression mode
CoreObject ifc_project = schema.instance("IfcProject")
    .set("GlobalId", uuid())
    .set("OwnerHistory", &owner_history)
    .set("Name", "Project")
    .set("RepresentationContexts", PropertyList{&context})
;
// Subscript operator:
ifc_project["UnitsInContext"] = schema.instance("IfcUnitAssignment", PropertyList{
    schema.instance("IfcSIUnit", EnumValue("*"), EnumValue("LENGTHUNIT"), nullptr, EnumValue("METRE"))
});
```


### Step file output

```c++

std::ofstream stream("output.ifc");
const Schema& schema = ifc2x3::schema();
writer.write_header(filename, schema);

writer.define_object(
    schema.instance("IfcCartesianPoint", PropertyList{0., 0., 0.})
);

writer.write_footer();
```
