
cmake_minimum_required (VERSION 3.7 FATAL_ERROR)

project(SimpleIFC VERSION 0.1.0 DESCRIPTION "Simple library to handle IFC / Express / Step" LANGUAGES CXX)

# C++
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

# Debug flags
if(CMAKE_BUILD_TYPE EQUAL "")
    set(CMAKE_BUILD_TYPE "Debug")
endif()
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -Wall -pedantic -Wextra")

# Targets
include_directories("${CMAKE_CURRENT_SOURCE_DIR}/include")

add_subdirectory(src lib)

add_custom_target(
    regen_ifc2x3
    COMMAND python3
        ${CMAKE_CURRENT_SOURCE_DIR}/generator/express_parser.py
        -g simpleifc
        -o ${CMAKE_CURRENT_SOURCE_DIR}/src/
    COMMAND mv ${CMAKE_CURRENT_SOURCE_DIR}/src/ifc2x3.h ${CMAKE_CURRENT_SOURCE_DIR}/include/simpleifc/
)
